import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.delay(2)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(2)

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP 1 span_1033CRF01-Penambahan Routing pada Router XL PB'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP2 span_1093CRF01Sonik Enhancement - User Mapping  ServicesUpgrade Version OS DB dan PHP SONIK'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP3 span_1121CRF01-PJ Heroes SMBCI user account'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP4 span_1156CRF01-23012019 - Merger - Penambahan IP Exclude DHCP untuk lantai 35 Treasury'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP5 span_1203CRF01-Server NGINX dan Minio Jenius'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP6 span_1237CRF01-Pergantian NAT Router RINTIS untuk SIT  UAT TWO Jenius'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP7 span_1277CRF01-Penambahan Routing Pada Router TASPEN Untuk Keperluan NEW EDAPEM'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP8 span_1334CRF01-Penambahan Routing Pada Router 3rd Party Fimi untuk Akses ke Nawakara'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP9 span_1380CRF01-07052019 - Pemindahan APIDEV apidevbtpncom ke DMZ Dev'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP10 span_1429CRF01-Repointing Brand RAC'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP11 span_1482CRF01-Upgrade IOS Version Batch 4 v10'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP12 span_1507CRF01-16072019 - DNS Forwarder sumitomobsmi corpbankbtpncoid'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP13 span_1552CRF01-TWO Clustering with Pacemaker'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP14 span_29032018 - F5 BigIQ - Implementasi F5 BigIQ fo F5 BigIP Configuration and Monitoring'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP15 span_951CRF01Digital Signature Visitor Management SystemServer Data Center Visitor Management System'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP16 span_988CRF01-06112018 - SMBCi - Akses network smbci 1030016 to akses internet via proxy internet'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP17 span_Cluster PostgreSQL J20'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP18 span_CRF01-27092018 - SOC Room - Koneksi CCTV dan Access Door untuk SOC Room lantai 20'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP19 span_CRF01-Penggantian SSL Certificate di F5 MDW untuk Aplikasi XL Tunai'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP20 span_CRF01-Upgrade OS F5 Switching DC DRC'), 60)

WebUI.delay(5)

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

for (WebElement link : mylist) {
    link.click()

    WebUI.delay(5)

    WebUI.mouseOver(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.delay(3)

    WebUI.switchToWindowIndex('1')

    WebUI.delay(3)

    // Save Main Document
    WebUI.comment('Main Document Section ')

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(7)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(3)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')
}

