import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.delay(2)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(2)

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP 1 span_1033CRF01-Penambahan Routing pada Router XL PB'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP2 span_1093CRF01Sonik Enhancement - User Mapping  ServicesUpgrade Version OS DB dan PHP SONIK'), 
    60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP3 span_1121CRF01-PJ Heroes SMBCI user account'), 60)

WebUI.scrollToElement(findTestObject('INFRA TERBARU/CP4 span_1156CRF01-23012019 - Merger - Penambahan IP Exclude DHCP untuk lantai 35 Treasury'), 
    60)

WebUI.delay(5)

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

for (WebElement link : mylist) {
    link.click()

    WebUI.delay(5)

    WebUI.mouseOver(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.delay(3)

    WebUI.switchToWindowIndex('1')

    WebUI.delay(3)

    // Save Main Document
    WebUI.comment('Main Document Section ')

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(7)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(3)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')
}

