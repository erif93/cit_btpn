import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode3_Complete/span_COS Integration 2019'), 60)

WebUI.click(findTestObject('Mode3_Complete/lblWOWImprovement'))

WebUI.mouseOver(findTestObject('ProjectID1-10/hrefActions'))

WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

WebUI.delay(3)

WebUI.switchToWindowIndex('1')

WebUI.delay(3)

// Save Main Document
WebUI.comment('Main Document Section ')

robot.keyPress(KeyEvent.VK_CONTROL)

robot.keyPress(KeyEvent.VK_P)

robot.keyRelease(KeyEvent.VK_P)

robot.keyRelease(KeyEvent.VK_CONTROL)

// Tawaf

WebUI.delay(7)

robot.keyPress(KeyEvent.VK_TAB)

robot.keyPress(KeyEvent.VK_TAB)

robot.keyRelease(KeyEvent.VK_TAB)

robot.keyRelease(KeyEvent.VK_TAB)

robot.keyPress(KeyEvent.VK_DOWN)

robot.keyRelease(KeyEvent.VK_DOWN)

WebUI.delay(1)

for(i=0;i<7;i++){
	
	robot.keyPress(KeyEvent.VK_TAB)
	
	robot.keyRelease(KeyEvent.VK_TAB)
	
}

WebUI.delay(6)

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.delay(5)

i = 0

//PCRF Section

List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/Release_Complete/lblDynamicRFCMode3Complete'), 30)

sizelist = RFCList.size()

for (WebElement RFC : RFCList) {
    WebUI.scrollToElement(findTestObject('Mode3_Complete/label_RFC MODE 3'), 60)

    WebElement context = RFCList.get(i)

    actions.moveToElement(context).perform()

    actions.contextClick(context).perform()

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_DOWN)

    robot.keyRelease(KeyEvent.VK_DOWN)

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    WebUI.switchToWindowIndex('1')
	
	WebUI.click(findTestObject('ProjectID1-10/hrefActions'))
	
	WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))
	
	WebUI.switchToWindowIndex('2')
	
	WebUI.delay(6)
	
	robot.keyPress(KeyEvent.VK_CONTROL)
	
	robot.keyPress(KeyEvent.VK_P)
	
	robot.keyRelease(KeyEvent.VK_P)
	
	robot.keyRelease(KeyEvent.VK_CONTROL)
	
	WebUI.delay(2)
	
	robot.keyPress(KeyEvent.VK_ENTER)
	
	robot.keyRelease(KeyEvent.VK_ENTER)
	
	WebUI.delay(2)
	
	robot.keyPress(KeyEvent.VK_ENTER)
	
	robot.keyPress(KeyEvent.VK_ENTER)
	
	robot.keyRelease(KeyEvent.VK_ENTER)
	
	robot.keyRelease(KeyEvent.VK_ENTER)
	
	WebUI.closeWindowIndex('2')
	
	WebUI.delay(1)
	
	WebUI.switchToWindowIndex('1')
	
	//Test or Implant Document
	
	List<WebElement> listImplantSITUAT = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)
	
		for (WebElement implantSITUAT : listImplantSITUAT) {
			implantSITUAT.click()
			
			WebUI.switchToWindowIndex(2)
			if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
				WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)

				WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)

				WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

				if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
					WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

					WebUI.delay(3)

					WebUI.closeWindowIndex('2')
					
					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
					WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

					WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

					WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

					WebUI.delay(25)

					WebUI.closeWindowIndex('2')

					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				} else if(WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'),3,FailureHandling.OPTIONAL)) {
					WebUI.delay(15)

					WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

					WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

					WebUI.delay(45)
					
					WebUI.delay(1)

					if(WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'),3,FailureHandling.OPTIONAL)){
						WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
					}
					WebUI.closeWindowIndex('2')

					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				} else {
				
						WebUI.closeWindowIndex('2')
				
						WebUI.switchToWindowIndex('1')
						
						WebUI.delay(1)
				}
			} else {
				if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
					WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

					WebUI.closeWindowIndex('2')

					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
					WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

					WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

					WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

					WebUI.delay(25)

					WebUI.closeWindowIndex('2')

					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)

				} else if(WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'),3,FailureHandling.OPTIONAL)) {
					WebUI.delay(15)

					WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

					WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

					WebUI.delay(45)

					if(WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'),3,FailureHandling.OPTIONAL)){
						WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
					}

					WebUI.closeWindowIndex('2')

					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				} else {
					WebUI.closeWindowIndex('2')
				
					WebUI.switchToWindowIndex('1')
					
					WebUI.delay(1)
				
				 }
					
	     }
			
}

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    CustomKeywords.'clickJS.clickUsingJS'(findTestObject('Mode3_Complete/Release_OnProgress/divBAST'), 60)

    i++

    WebUI.delay(1)
}

