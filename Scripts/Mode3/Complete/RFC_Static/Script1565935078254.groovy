import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory


WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()
//RFC
WebUI.comment('RFC')

if (WebUI.verifyElementPresent(findTestObject('Mode3_Complete/hrefDynamicRFCMode3'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/hrefDynamicRFCMode3'),
		30)

	c = 7

	sizelist = RFCList.size()

	for (WebElement RFC : RFCList) {
		WebUI.scrollToElement(findTestObject('Mode3_Complete/span_RFCMode3'), 60)

		WebUI.delay(1)

		WebElement context = RFCList.get(c)

		actions.moveToElement(context).perform()

		WebUI.delay(1)

		actions.contextClick(context).perform()

		c++

		robot.keyPress(KeyEvent.VK_DOWN)

		robot.keyRelease(KeyEvent.VK_DOWN)

		WebUI.delay(1)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		WebUI.delay(2)

		WebUI.switchToWindowIndex('1')

		WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

		WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

		WebUI.switchToWindowIndex('2')

		WebUI.delay(3)

		robot.keyPress(KeyEvent.VK_CONTROL)

		robot.keyPress(KeyEvent.VK_P)

		robot.keyRelease(KeyEvent.VK_P)

		robot.keyRelease(KeyEvent.VK_CONTROL)

		WebUI.delay(8)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		WebUI.delay(3)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		WebUI.closeWindowIndex('2')

		WebUI.delay(1)

		WebUI.switchToWindowIndex('1')

		if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
			List<WebElement> listDocumentRFC = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'),
				30)

			for (WebElement documentRFC : listDocumentRFC) {
				documentRFC.click()

				WebUI.delay(1)
			}
		} else {
			println('Document Persetujuan Not Found')
		}
		
		if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 3, FailureHandling.OPTIONAL)) {
			List<WebElement> listImplantSITUAT = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

			for (WebElement implantSITUAT : listImplantSITUAT) {
				WebUI.delay(2)

				implantSITUAT.click()

				WebUI.delay(1)

				WebUI.switchToWindowIndex('2')

				if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
					WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

					WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

					WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

					if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'),
						3, FailureHandling.OPTIONAL)) {
						WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

						WebUI.delay(3)

						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
						WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

						WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

						WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

						WebUI.delay(20)

						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
						WebUI.delay(10)

						WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

						WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

						
						WebUI.delay(35)
						
						if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
							WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
						}
						
						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					} else {
						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					}
				} else {
					if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'),
						3, FailureHandling.OPTIONAL)) {
						WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
						WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

						WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

						WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

						WebUI.delay(15)

						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					} else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
					
								WebUI.delay(10)
					
								WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))
					
								if(WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblExportDetails'),3,FailureHandling.OPTIONAL)) {
													   
													   
										WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))
					
										WebUI.delay(35)
					
										if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
										WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
											 }
												
												WebUI.closeWindowIndex('2')
					
												WebUI.switchToWindowIndex('1')
												
											}   else {
													   
												WebUI.closeWindowIndex('2')
													   
												WebUI.switchToWindowIndex('1')
												
									}
					} else {
						WebUI.closeWindowIndex('2')

						WebUI.switchToWindowIndex('1')
					}
				}
			}
			
			WebUI.closeWindowIndex('1')

			WebUI.switchToWindowIndex('0')

			CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

			WebUI.delay(2 //end RFC For Section
				)
		} else {
			println('Document SIT/UAT Not Found')

			WebUI.delay(2)

			WebUI.closeWindowIndex('1')

			WebUI.switchToWindowIndex('0')

			CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

			WebUI.delay(1)
		}
	}
} else {
	println(' RFC Not Found')
}

if (WebUI.verifyElementPresent(findTestObject('Extension/lblPDFExtension'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> listPDF = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblPDFExtension'), 30)

	for (WebElement documentPDF : listPDF) {
		documentPDF.click()
	}
}

if (WebUI.verifyElementPresent(findTestObject('Extension/lblXlsExtension'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> listXLS = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblXlsExtension'), 30)

	for (WebElement documentXLS : listXLS) {
		documentXLS.click()
	}
}

if (WebUI.verifyElementPresent(findTestObject('Extension/lblPPTExtension'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> listPPT = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblPPTExtension'), 30)

	for (WebElement documentPPT : listPPT) {
		documentPPT.click()
	}
}

if (WebUI.verifyElementPresent(findTestObject('Extension/lblDocExtension'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> listDOC = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblDocExtension'), 30)

	for (WebElement documentDOC : listDOC) {
		documentDOC.click()
	}
}

if (WebUI.verifyElementPresent(findTestObject('Extension/lblMsgExtension'), 3, FailureHandling.OPTIONAL)) {
	List<WebElement> listMSG = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblMsgExtension'), 30)

	for (WebElement documentMSG : listMSG) {
		documentMSG.click()
	}
}