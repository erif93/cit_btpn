import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.delay(15)

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

//Backlog SS Section
if (WebUI.verifyElementPresent(findTestObject('Mode3_Complete/hreftDynamicProductBacklog'), 2, FailureHandling.OPTIONAL)) {
    List<WebElement> backLogList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/hreftDynamicProductBacklog'), 
        30)

    for (WebElement backlog : backLogList) {
        backlog.click()

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        a = 0

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(a + '_MainBacklog.png', FailureHandling.STOP_ON_FAILURE)

            a++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        } else {
            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(a + '_MainBacklog.png', FailureHandling.STOP_ON_FAILURE)

            a++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        }
    }
} else {
    println('Backlog Not Found')
}

//End Back log SS Section
//SprintBacklog Section
if (WebUI.verifyElementPresent(findTestObject('Mode3_Complete/hreftDynamicSprintBacklog'), 2, FailureHandling.OPTIONAL)) {
    List<WebElement> sprintBacklogList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/hreftDynamicSprintBacklog'), 
        30)

    for (WebElement sprintBacklog : sprintBacklogList) {
        sprintBacklog.click()

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        b = 0

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(b + '_SprintBacklog.png', FailureHandling.STOP_ON_FAILURE)

            b++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        } else {
            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(b + '_SprintBacklog.png', FailureHandling.STOP_ON_FAILURE)

            b++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        }
    }
} else {
    println('SprintBacklog Not Found')
}

//end of sprint backlog section
//RFC
WebUI.comment('RFC')

WebUI.scrollToElement(findTestObject('Mode3_Complete/span_RFCMode3'), 60)

if (WebUI.verifyElementPresent(findTestObject('Mode3_Complete/hrefDynamicRFCMode3'), 3, FailureHandling.OPTIONAL)) {
    List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/hrefDynamicRFCMode3'), 30)

    c = 0

    for (WebElement RFC : RFCList) {
        WebUI.delay(1)

        WebElement context = RFCList.get(c)

        actions.moveToElement(context).perform()

        WebUI.delay(1)

        actions.contextClick(context).perform()

        c++

        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)

        WebUI.delay(1)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(2)

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('Mode3_Complete/hrefDynamicBacklogRFC'), 3, FailureHandling.OPTIONAL)) {
            List<WebElement> backlogRFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode3_Complete/hrefDynamicBacklogRFC'), 
                30)

            for (WebElement backlogRFC : backlogRFCList) {
                backlogRFC.click()

                WebUI.delay(1)

                WebUI.switchToWindowIndex('2')

                d = 0

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

                    WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

                    WebUI.delay(10)

                    CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(d + '_RFCBacklog.png', FailureHandling.STOP_ON_FAILURE)

                    d++

                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')
                } else {
                    WebUI.delay(10)

                    CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(d + '_RFCBacklog.png', FailureHandling.STOP_ON_FAILURE)

                    d++

                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')
                }
            }
            
            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

            WebUI.delay(2 //end RFC For Section
                )
        } else {
            println('SprintBacklog Not Found')

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

            WebUI.delay(2 //end RFC For Section
                )
        }
    }
} else {
    println('RFC Not Found')
}

