import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.delay(5)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.mouseOver(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode2_Complete/lbl034Ketersambungan XL Tunai'), 0)

WebUI.scrollToElement(findTestObject('Mode2_Complete/lbl058Enhancement Wealth Management System - Bancassurance  Mutual Funds'), 
    0)

WebUI.scrollToElement(findTestObject('Mode2_Complete/lbl082Layanan BTPN Wow Payroll Phase 1'), 0)

WebUI.delay(3)

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

a = 0

b = 0

for (WebElement link : mylist) {
    mylist.get(b).click()

    WebUI.delay(3)

    b++

    if (WebUI.verifyElementPresent(findTestObject('Mode2_Complete/hrefDynamicWBSMode2'), 3, FailureHandling.OPTIONAL)) {
        WebUI.click(findTestObject('Mode2_Complete/hrefDynamicWBSMode2'))

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(a + '_wbs_mode2.png', FailureHandling.STOP_ON_FAILURE)

            a++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        } else {
            WebUI.delay(10)

            CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(a + '_wbs_mode2.png', FailureHandling.STOP_ON_FAILURE)

            a++

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        }
    } else {
        println('Link Not Found')
    }
}

