import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

WebUI.delay(3)

//PCRF
WebUI.comment('PCRF Section')

if (WebUI.verifyElementPresent(findTestObject('Mode2_Complete/lblPCRFMode2'), 1, FailureHandling.OPTIONAL)) {
    List<WebElement> PCRFList = WebUiCommonHelper.findWebElements(findTestObject('Mode2_Complete/lblPCRFMode2'), 30)

    d = 1

    sizePCRF = PCRFList.size()

    for (WebElement PCRFMain : PCRFList) {
        WebElement contextPCRF = PCRFList.get(d)

        WebUI.scrollToElement(findTestObject('Mode2_Complete/span_PCRF'), 60)

        WebUI.delay(2)

        actions.moveToElement(contextPCRF).perform()

        actions.contextClick(contextPCRF).perform()

        WebUI.delay(2)

        d++

        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)

        WebUI.delay(1)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

        WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

        WebUI.switchToWindowIndex('2')

        WebUI.delay(5)

        robot.keyPress(KeyEvent.VK_CONTROL)

        robot.keyPress(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_CONTROL)

        WebUI.delay(8)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.closeWindowIndex('2')

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 1, FailureHandling.OPTIONAL)) {
            List<WebElement> listPCRF = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 
                30)

            for (WebElement PCRF : listPCRF) {
                PCRF.click()

                WebUI.delay(1)
            }
        } else {
            println('Document Persetujuan Not Found')
        }
        
        if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 1, FailureHandling.OPTIONAL)) {
            List<WebElement> listBRDUCS = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

            for (WebElement BRDUCS : listBRDUCS) {
                BRDUCS.click()

                WebUI.delay(2)

                WebUI.switchToWindowIndex('2')

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0003)

                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0003)

                    WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(25)

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    }
                } else {
                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(15)

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    }
                }
            }
            
            WebUI.closeWindowIndex('1')

            WebUI.delay(1)

            WebUI.switchToWindowIndex('0')

            CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

            WebUI.delay(1)
        } else {
            println('Document BRD and UCS Not Found')

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            WebUI.delay(1)

            CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

            WebUI.delay(1)
        }
    }
} else {
    println(' PCRF Not Found')
}

