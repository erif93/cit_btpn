import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.delay(3)

WebUI.click(findTestObject('ProjectID1-10/lblProjectID002'))

WebUI.delay(5)

if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblPCRF'),3, FailureHandling.OPTIONAL)) {
    WebUI.mouseOver(findTestObject('Detail_ProjectID/lblPCRF'))

     WebUI.rightClick(findTestObject('Detail_ProjectID/lblPCRF'))

     robot.keyPress(KeyEvent.VK_DOWN)

     robot.keyRelease(KeyEvent.VK_DOWN)

     WebUI.delay(3)

     robot.keyPress(KeyEvent.VK_ENTER)

     robot.keyPress(KeyEvent.VK_ENTER)

     robot.keyRelease(KeyEvent.VK_ENTER)

     robot.keyRelease(KeyEvent.VK_ENTER)

     WebUI.delay(3)

     WebUI.switchToWindowIndex('1')
	
	

    if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listPCRF = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 30)

        for (WebElement PCRF : listPCRF) {
            PCRF.click()

            WebUI.delay(1)
        }
    } else {
        println('Document Persetujuan Not Found')
    }
    
    if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listBRDUCS = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

        for (WebElement BRDUCS : listBRDUCS) {
            BRDUCS.click()

            WebUI.delay(2)

            WebUI.switchToWindowIndex('1')

            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
                WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)

                WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

				if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
					WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
	
					WebUI.closeWindowIndex('1')
	
					WebUI.switchToWindowIndex('0')
					
					WebUI.back()
					
					WebUI.delay(2)
					
					WebUI.click(findTestObject('btnTreeActions'))
					
					WebUI.click(findTestObject('lblExpandAll'))
				}else  {
					WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))
	
					WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))
	
					WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
	
					WebUI.delay(25)
	
					WebUI.closeWindowIndex('1')
	
					WebUI.switchToWindowIndex('0')
					
					WebUI.back()
					
					WebUI.delay(2)
					
					WebUI.click(findTestObject('btnTreeActions'))
					
					WebUI.click(findTestObject('lblExpandAll'))
				}
            } else {
					if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
						WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

						WebUI.closeWindowIndex('1')

						WebUI.switchToWindowIndex('0')
						
						WebUI.back()
						
						WebUI.delay(2)
						
						WebUI.click(findTestObject('btnTreeActions'))
						
						WebUI.click(findTestObject('lblExpandAll'))
						
					} else  {
						WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

						WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

						WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

						WebUI.delay(15)

						WebUI.closeWindowIndex('1')

						WebUI.switchToWindowIndex('0')
						
						WebUI.back()
						
						WebUI.delay(2)
						
						WebUI.click(findTestObject('btnTreeActions'))
						
						WebUI.click(findTestObject('lblExpandAll'))
					}
            	}
        	}
    } else {
        println('Document BRD and UCS Not Found')
		
		WebUI.back()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('btnTreeActions'))
		
		WebUI.click(findTestObject('lblExpandAll'))
		
    	}
	} 
else {
    println(' PCRF Not Found')
}

