import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.interactions.Actions as Actions






WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.click(findTestObject('Test/span_Check Dedup Rekening Pelajar Phase 2'))

WebUI.delay(5)

i=0

List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode1_Complete/lblDynamicRFCMode1Complete'), 30)

sizelist = RFCList.size()

for (WebElement RFC : RFCList) {
    WebUI.scrollToElement(findTestObject('Mode1_Complete/lblRFCMode1'), 60)

    WebElement context = RFCList.get(i)

    

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_DOWN)

    robot.keyRelease(KeyEvent.VK_DOWN)

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    WebUI.switchToWindowIndex('1')

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.switchToWindowIndex('2')

    WebUI.delay(6)

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(2)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('2')

    WebUI.delay(1)

    WebUI.switchToWindowIndex('1')

    //Test or Implant Document
    List<WebElement> listImplantSITUAT = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

    for (WebElement implantSITUAT : listImplantSITUAT) {
        implantSITUAT.click()

        WebUI.switchToWindowIndex(2)

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.delay(3)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(25)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.delay(15)

                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(45)

                WebUI.delay(1)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else {
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            }
        } else {
            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(25)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.delay(15)

                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(45)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else {
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            }
        }
    }
    
    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

    i++

    WebUI.delay(1)
}WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()


WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.click(findTestObject('Test/span_Check Dedup Rekening Pelajar Phase 2'))

WebUI.delay(5)

i=0

List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode1_Complete/lblDynamicRFCMode1Complete'), 30)

sizelist = RFCList.size()

for (WebElement RFC : RFCList) {
    WebUI.scrollToElement(findTestObject('Mode1_Complete/lblRFCMode1'), 60)

    WebElement context = RFCList.get(i)

    actions.moveToElement(context).perform()

    actions.contextClick(context).perform()

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_DOWN)

    robot.keyRelease(KeyEvent.VK_DOWN)

    WebUI.delay(1)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    WebUI.switchToWindowIndex('1')

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.switchToWindowIndex('2')

    WebUI.delay(6)

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(2)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('2')

    WebUI.delay(1)

    WebUI.switchToWindowIndex('1')

    //Test or Implant Document
    List<WebElement> listImplantSITUAT = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

    for (WebElement implantSITUAT : listImplantSITUAT) {
        implantSITUAT.click()

        WebUI.switchToWindowIndex(2)

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.delay(3)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(25)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.delay(15)

                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(45)

                WebUI.delay(1)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else {
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            }
        } else {
            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(25)

                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.delay(15)

                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(45)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            } else {
                WebUI.closeWindowIndex('2')

                WebUI.switchToWindowIndex('1')

                WebUI.delay(1)
            }
        }
    }
    
    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)

    i++

    WebUI.delay(1)
}
