import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

int counter = 0

if (WebUI.verifyElementPresent(findTestObject('RQM/a_NextDisabled'), 2, FailureHandling.OPTIONAL)) {
    List<WebElement> testplanList = WebUiCommonHelper.findWebElements(findTestObject('RQM/hrefTestPlanLink'), 30)

    for (WebElement testplan : testplanList) {
        WebElement context = testplanList.get(counter)

        actions.moveToElement(context).perform()

        actions.contextClick(context).perform()

        WebUI.delay(1)

        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)

        WebUI.delay(1)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(1)

        counter++

        WebUI.switchToWindowIndex('1')

        WebUI.delay(1)

        //TestCase
        WebUI.click(findTestObject('RQM/testplan/hrefTestCases'))

        if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/testCaseChild'), 2, FailureHandling.OPTIONAL)) {
            if (WebUI.verifyElementPresent(findTestObject('RQM/a_NextDisabled'), 2, FailureHandling.OPTIONAL)) {
                int counterTestCase = 0

                List<WebElement> testcaseChildList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/testCaseChild'), 
                    30)

                for (WebElement child : testcaseChildList) {
                    WebElement childContext = testcaseChildList.get(counterTestCase)

                    actions.moveToElement(childContext).perform()

                    actions.contextClick(childContext).perform()

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_DOWN)

                    robot.keyRelease(KeyEvent.VK_DOWN)

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    WebUI.delay(1)

                    WebUI.switchToWindowIndex('2')

                    counterTestCase++

                    // click attachment on Testcase page
                    List<WebElement> attachmentTcChildList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                        30)

                    int attachmentTcSize = attachmentTcChildList.size()

                    println(attachmentTcSize)

                    attachmentTcChildList.get(attachmentTcSize - 1).click()

                    // check and loop every TC attachment
                    if (WebUI.verifyElementNotPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                        2, FailureHandling.OPTIONAL)) {
                        println('No attachment')

                        WebUI.delay(3)
                    } else {
                        List<WebElement> attachmentTcList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                            30)

                        for (WebElement attachmentTc : attachmentTcList) {
                            attachmentTc.click()

                            WebUI.delay(1)
                        }
                    }
                    
                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')

                    WebUI.delay(1)
                } // test case more than 1 pagination
            } else {
                WebUI.callTestCase(findTestCase('RQM/Planning/Open_Testcase_More_Than_1_Pagination'), [:], FailureHandling.STOP_ON_FAILURE)
            }
        }
        
        // check Test Suite
        WebUI.click(findTestObject('RQM/testplan/hrefTestSuite'))

        int counterTestSuite = 0

        if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/testSuiteChild'), 2, FailureHandling.OPTIONAL)) {
            if (WebUI.verifyElementPresent(findTestObject('RQM/a_NextDisabled'), 2, FailureHandling.OPTIONAL)) {
                List<WebElement> testsuiteChildList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/testSuiteChild'), 
                    30)

                for (WebElement tsChild : testsuiteChildList) {
                    WebElement child_TsContext = testsuiteChildList.get(counterTestSuite)

                    actions.moveToElement(child_TsContext).perform()

                    actions.contextClick(child_TsContext).perform()

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_DOWN)

                    robot.keyRelease(KeyEvent.VK_DOWN)

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    WebUI.delay(1)

                    WebUI.switchToWindowIndex('2')

                    counterTestSuite++

                    // check TS Attachment
                    WebUI.delay(8)

                    List<WebElement> attachmentTextList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                        30)

                    int attachmentSize = attachmentTextList.size()

                    println(attachmentSize)

                    attachmentTextList.get(attachmentSize - 1).click()

                    // check and loop every TS attachment
                    if (WebUI.verifyElementNotPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                        2, FailureHandling.OPTIONAL)) {
                        println('No attachment')
                    } else {
                        List<WebElement> childAttachmentTsList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                            30)

                        for (WebElement childTsAtt : childAttachmentTsList) {
                            childTsAtt.click()
                        }
                    }
                    
                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')

                    WebUI.delay(1)
                } // ts more than 1 pagination
            } else {
                WebUI.callTestCase(findTestCase('RQM/Planning/Open_Testsuite_More_Than_1_Pagination'), [:], FailureHandling.STOP_ON_FAILURE)
            }
        }
        
        //cehck Main Attachment
        WebUI.click(findTestObject('RQM/testplan/hrefAttachments'))

        if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 2, FailureHandling.OPTIONAL)) {
            List<WebElement> attachmentList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                30)

            for (WebElement attachment : attachmentList) {
                attachment.click()

                WebUI.delay(1)
            }
        }
        
        WebUI.closeWindowIndex('1')

        WebUI.switchToWindowIndex('0')

        WebUI.delay(1)
    }
} else {
    println('more than 1 pagination')

    boolean btnDisabled = WebUI.verifyElementNotPresent(findTestObject('RQM/a_NextDisabled'), 60)

    

    while (btnDisabled) {
		
		int counter_child = 0
		
		int counter_mainchild = 0
		
        List<WebElement> testplanList = WebUiCommonHelper.findWebElements(findTestObject('RQM/hrefTestPlanLink'), 30)

        for (WebElement testplan : testplanList) {
            WebElement context = testplanList.get(counter_mainchild)

            actions.moveToElement(context).perform()

            actions.contextClick(context).perform()

            WebUI.delay(1)

            robot.keyPress(KeyEvent.VK_DOWN)

            robot.keyRelease(KeyEvent.VK_DOWN)

            WebUI.delay(1)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            WebUI.delay(1)

            counter_mainchild++

            WebUI.switchToWindowIndex('1')

            WebUI.delay(1)

            WebUI.click(findTestObject('RQM/testplan/hrefTestCases'))
			
			//test case

            if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/testCaseChild'), 2, FailureHandling.OPTIONAL)) {
				
				if (WebUI.verifyElementPresent(findTestObject('RQM/a_NextDisabled'), 2, FailureHandling.OPTIONAL)) {
				
                int counterTestCaseElse = 0

                List<WebElement> testcaseChildList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/testCaseChild'), 
                    30)

                for (WebElement child : testcaseChildList) {
                    WebElement childContext = testcaseChildList.get(counterTestCaseElse)

                    actions.moveToElement(childContext).perform()

                    actions.contextClick(childContext).perform()

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_DOWN)

                    robot.keyRelease(KeyEvent.VK_DOWN)

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    WebUI.delay(1)

                    WebUI.switchToWindowIndex('2')

                    WebUI.delay(1)

                    counterTestCaseElse++

                    List<WebElement> attachmentTcChildList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                        30)

                    int attachmentTcSize = attachmentTcChildList.size()

                    println(attachmentTcSize)

                    attachmentTcChildList.get(attachmentTcSize - 1).click()

                    if (WebUI.verifyElementNotPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                        2, FailureHandling.OPTIONAL)) {
                        println('No attachment')
                    } else {
                        List<WebElement> attachmentTcList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                            30)

                        for (WebElement attachmentTc : attachmentTcList) {
                            attachmentTc.click()

                            WebUI.delay(1)
                        }
                    }
                    
                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')

                    WebUI.delay(1)
                }
            } else {
	   
			WebUI.callTestCase(findTestCase('RQM/Planning/Open_Testcase_More_Than_1_Pagination'), [:], FailureHandling.STOP_ON_FAILURE)
	   
          }
       }   
				
				
			//test suite
            
            WebUI.click(findTestObject('RQM/testplan/hrefTestSuite'))

            int counterTestSuiteElse = 0

            if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/testSuiteChild'), 2, FailureHandling.OPTIONAL)) {
				
				if (WebUI.verifyElementPresent(findTestObject('RQM/a_NextDisabled'), 2, FailureHandling.OPTIONAL)) {
					List<WebElement> testsuiteChildList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/testSuiteChild'), 30)

					for (WebElement tsChild : testsuiteChildList) {
                    WebElement child_TsContext = testsuiteChildList.get(counterTestSuiteElse)

                    actions.moveToElement(child_TsContext).perform()

                    actions.contextClick(child_TsContext).perform()

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_DOWN)

                    robot.keyRelease(KeyEvent.VK_DOWN)

                    WebUI.delay(1)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyPress(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    robot.keyRelease(KeyEvent.VK_ENTER)

                    WebUI.delay(1)

                    WebUI.switchToWindowIndex('2')

                    counterTestSuiteElse++

                    WebUI.delay(8)

                    List<WebElement> attachmentTextList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                        30)

                    int attachmentSize = attachmentTextList.size()

                    println(attachmentSize)

                    attachmentTextList.get(attachmentSize - 1).click()

                    if (WebUI.verifyElementNotPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                        2, FailureHandling.OPTIONAL)) {
                        println('No attachment')
                    } else {
                        List<WebElement> childAttachmentTsList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'), 
                            30)

                        for (WebElement childTsAtt : childAttachmentTsList) {
                            childTsAtt.click()
                        }
                    }
                    
                    WebUI.closeWindowIndex('2')

                    WebUI.switchToWindowIndex('1')
                }
            } else {
			
					WebUI.callTestCase(findTestCase('RQM/Planning/Open_Testsuite_More_Than_1_Pagination'), [:], FailureHandling.STOP_ON_FAILURE)
            }
			
        }
            
			
			//attachments
            WebUI.click(findTestObject('RQM/testplan/hrefAttachments'))

            if (WebUI.verifyElementPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 2, FailureHandling.OPTIONAL)) {
                List<WebElement> attachmentList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 
                    30)

                for (WebElement attachment : attachmentList) {
                    attachment.click()

                    WebUI.delay(1)
                }
            }
            
            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            WebUI.delay(1)
        }
        
        if (WebUI.verifyElementPresent(findTestObject('RQM/span_NextActive'), 3, FailureHandling.OPTIONAL)) {
            WebUI.click(findTestObject('RQM/span_NextActive'))
        } else {
		
            break
        }
    }
}

