import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory


WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()



boolean btnNextTcPagination = WebUI.verifyElementNotPresent(findTestObject('RQM/a_NextDisabled'), 60)


while(btnNextTcPagination) {
	
	int tcpagination = 0
	
	List<WebElement> testcaseChildPagination = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/testCaseChild'),30)

		for (WebElement childPagination : testcaseChildPagination) {
	
				WebElement childContextPagination = testcaseChildPagination.get(tcpagination)

				actions.moveToElement(childContextPagination).perform()

				actions.contextClick(childContextPagination).perform()

				WebUI.delay(1)

				robot.keyPress(KeyEvent.VK_DOWN)

				robot.keyRelease(KeyEvent.VK_DOWN)

				WebUI.delay(1)

				robot.keyPress(KeyEvent.VK_ENTER)

				robot.keyPress(KeyEvent.VK_ENTER)

				robot.keyRelease(KeyEvent.VK_ENTER)

				robot.keyRelease(KeyEvent.VK_ENTER)

				WebUI.delay(1)

				WebUI.switchToWindowIndex('2')

				tcpagination++

	// check TC Attachment
				List<WebElement> attachmentTcChildList = WebUiCommonHelper.findWebElements(findTestObject('RQM/testplan/hrefAttachments'),30)

				int attachmentTcSize = attachmentTcChildList.size()

				println(attachmentTcSize)

				attachmentTcChildList.get(attachmentTcSize - 1).click()

	// check and loop every TC attachment

				if (WebUI.verifyElementNotPresent(findTestObject('Object Repository/RQM/testplan/MainAttachments'), 2, FailureHandling.OPTIONAL)) {
					
					println('No attachment')

					WebUI.delay(2)
				} else {
				
				List<WebElement> attachmentTcList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/RQM/testplan/MainAttachments'),30)

				for (WebElement attachmentTc : attachmentTcList) {
					
					attachmentTc.click()

					WebUI.delay(1)
		}
	 }
	
	WebUI.closeWindowIndex('2')

	WebUI.switchToWindowIndex('1')
	
	WebUI.delay(1)
}
		
		if (WebUI.verifyElementPresent(findTestObject('RQM/span_NextActive'), 3, FailureHandling.OPTIONAL)) {
			
			WebUI.click(findTestObject('RQM/span_NextActive'))
		} else {
		
			break
		}
}