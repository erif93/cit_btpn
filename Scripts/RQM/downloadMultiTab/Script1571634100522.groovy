import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

WebUI.delay(20)

//WebUI.click(findTestObject('RQM/span_Explore Project'))

//WebUI.delay(1)

//WebUI.click(findTestObject('RQM/span_Construction'))

//WebUI.delay(1)

//WebUI.click(findTestObject('RQM/span_BrowseTest Cases'))



//WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

//WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/RQM/td_Export All Pages'))

//WebUI.delay(1)

//WebUI.back()

boolean btnDisabled = WebUI.verifyElementNotPresent(findTestObject('RQM/a_NextDisabled'), 60)

WebUI.delay(15)

while (btnDisabled) {
	List<WebElement> testplanList = WebUiCommonHelper.findWebElements(findTestObject('RQM/CILOK'), 30)

	c = 0
	n = 0

	for (WebElement testplan : testplanList) {
		WebElement context = testplanList.get(c)

		actions.moveToElement(context).perform()

		actions.contextClick(context).perform()

		WebUI.delay(1)

		c++ 

		robot.keyPress(KeyEvent.VK_DOWN)

		robot.keyRelease(KeyEvent.VK_DOWN)

		WebUI.delay(1)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyPress(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		robot.keyRelease(KeyEvent.VK_ENTER)

		WebUI.delay(1)
		
	}
		

				WebUI.switchToWindowIndex('10')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
				
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
				
					WebUI.delay(2)
				
					robot.keyPress(KeyEvent.VK_CONTROL)
				
					robot.keyPress(KeyEvent.VK_C)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
				
					robot.keyRelease(KeyEvent.VK_C)
				
					WebUI.delay(1)
				
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
				
					WebUI.delay(1)
				
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
				
					robot.keyRelease(KeyEvent.VK_S)
				
					WebUI.delay(2)
				
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
				
					robot.keyRelease(KeyEvent.VK_V)
				
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
				
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
					
				WebUI.switchToWindowIndex('9')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
				
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
				
					WebUI.delay(2)
				
					robot.keyPress(KeyEvent.VK_CONTROL)
				
					robot.keyPress(KeyEvent.VK_C)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
				
					robot.keyRelease(KeyEvent.VK_C)
				
					WebUI.delay(1)
				
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
				
					WebUI.delay(1)
				
					robot.keyPress(KeyEvent.VK_CONTROL)
				
					robot.keyPress(KeyEvent.VK_S)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
				
					robot.keyPress(KeyEvent.VK_V)
				
					robot.keyRelease(KeyEvent.VK_CONTROL)
				
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
					
				WebUI.switchToWindowIndex('8')
					
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
					
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
				
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
					
				WebUI.switchToWindowIndex('7')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('6')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('5')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('4')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('3')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('2')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(2)
				
				WebUI.switchToWindowIndex('1')
				
					WebUI.verifyElementPresent(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
				
					WebUI.delay(1)
					
					WebUI.doubleClick(findTestObject('RQM/DOUBLEKLIK'))
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_C)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_C)
					
					WebUI.delay(1)
					
					WebUI.click(findTestObject('RQM/LAHANKOSONG'))
					
					WebUI.delay(1)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_S)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_S)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_CONTROL)
					
					robot.keyPress(KeyEvent.VK_V)
					
					robot.keyRelease(KeyEvent.VK_CONTROL)
					
					robot.keyRelease(KeyEvent.VK_V)
					
					WebUI.delay(2)
					
					robot.keyPress(KeyEvent.VK_ENTER)
					
					robot.keyRelease(KeyEvent.VK_ENTER)
					
					WebUI.delay(12)
		
		// WebUI.delay(15)
		//WebUI.waitForElementVisible(findTestObject('Object Repository/Detail_ProjectID/refresh'), 60)
		
		// UNTUK DOWNLOAD PDF

		//WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))
		
		//WebUI.delay(1)

		//WebUI.click(findTestObject('Object Repository/RQM/td_Export Summary'))
		
		//WebUI.delay(1)
		
		// UNTUK LANGSUNG DOWNLOAD
		
		//if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
			//WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))}

		WebUI.closeWindowIndex('10')
		WebUI.delay(1)
		WebUI.closeWindowIndex('9')
		WebUI.delay(1)
		WebUI.closeWindowIndex('8')
		WebUI.delay(1)
		WebUI.closeWindowIndex('7')
		WebUI.delay(1)
		WebUI.closeWindowIndex('6')
		WebUI.delay(1)
		WebUI.closeWindowIndex('5')
		WebUI.delay(1)
		WebUI.closeWindowIndex('4')
		WebUI.delay(1)
		WebUI.closeWindowIndex('3')
		WebUI.delay(1)
		WebUI.closeWindowIndex('2')
		WebUI.delay(1)
		WebUI.closeWindowIndex('1')
		WebUI.delay(1)
		WebUI.switchToWindowIndex('0')
		
		WebUI.delay(1)
	
	
	WebUI.click(findTestObject('RQM/span_NextActive'))

	WebUI.delay(1)

	if (WebUI.verifyElementVisible(findTestObject('RQM/a_NextDisabled'), FailureHandling.OPTIONAL)) {
		break
	} else {
		println('loop again')
	}
}

