import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String chromeDriver75 = 'D:\\driver75\\chromedriver.exe'

System.setProperty('webdriver.chrome.driver', chromeDriver75)

WebDriver driver = new ChromeDriver()

DriverFactory.changeWebDriver(driver)

WebUI.maximizeWindow()

WebUI.navigateToUrl('cit')

WebUI.click(findTestObject('Login RQM/div_E-Mail_ficon tebutton'))

WebUI.click(findTestObject('Login RQM/a_Rational Quality Manager'))

WebUI.setText(findTestObject('Login RRC/RRC input_User ID_j_username'), GlobalVariable.usernameFocalPointCIT0002)

WebUI.setText(findTestObject('Login RRC/RRC input_Password_j_password'), GlobalVariable.passwordFocalPointCIT0002)

WebUI.click(findTestObject('Login RRC/RRC button_Log In'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

