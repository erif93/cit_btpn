import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Robot robot = new Robot()

WebUI.delay(9)

//WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/span_Enhancement Reconcile System Phase II'), 60)

//WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/span_ENHANCEMENT CUSTOMER RELATIONSHIP MANAGEMENT'), 60)

int a = 0

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

for (WebElement mainList : mylist) {
    mylist.get(a).click()

    a++

    List<WebElement> riskList = WebUiCommonHelper.findWebElements(findTestObject('Mode1_Complete/hrefDynamicRiskAssesment'), 
        30)

    for (WebElement risk : riskList) {
        risk.click()

        WebUI.switchToWindowIndex('1')

        WebUI.delay(1)

        WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

        WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

        WebUI.switchToWindowIndex('2')

        WebUI.delay(6)

        robot.keyPress(KeyEvent.VK_CONTROL)

        robot.keyPress(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_CONTROL)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.closeWindowIndex('2')

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        //Main Document Risk Section
        WebUI.closeWindowIndex('1')

        WebUI.switchToWindowIndex('0')
    }
}

