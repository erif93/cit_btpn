import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.delay(5)

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

List<WebElement> secondList = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

WebElement thirdList = secondList.get(2)

for (WebElement list : mylist) {
//	
//	greenIcon = link.getAttribute('class')
//	
//	
//	
//	TestObject to = new TestObject()
//	
//	to.addProperty('tag', ConditionType.EQUALS, 'span')
//	
//	to.addProperty('class', ConditionType.EQUALS, greenIcon)
//	
//	WebUI.click(to)
//	
//    WebUI.click(findTestObject('btnTestObjectGreenIcon'))
//
//    WebUI.delay(5)

//    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))
//
//    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))
//
//    WebUI.delay(3)
//
//    WebUI.switchToWindowIndex('1')
//
//    WebUI.comment('Main Document Section ')
//
//    robot.keyPress(KeyEvent.VK_CONTROL)
//
//    robot.keyPress(KeyEvent.VK_P)
//
//    robot.keyRelease(KeyEvent.VK_P)
//
//    robot.keyRelease(KeyEvent.VK_CONTROL)
//
//    WebUI.delay(15)
//
//    robot.keyPress(KeyEvent.VK_ENTER)
//	
//	WebUI.delay(3)
//
//    robot.keyRelease(KeyEvent.VK_ENTER)
//
//    WebUI.delay(3)
//
//    WebUI.closeWindowIndex('1')
//
//    WebUI.switchToWindowIndex('0')
//
//    List<WebElement> listDocument = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 30)
//
//    WebUI.comment('Download Direct Document Section')
//
//    for (WebElement document : listDocument) {
//        document.click()
//
//        WebUI.delay(3)
//    }
//    
//    WebUI.comment(' BRD, WBS , SIT , UAT Section')
//
//    List<WebElement> rlLink = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)
//
//    for (WebElement rLink : rlLink) {
//        rLink.click()
//
//        WebUI.delay(2)
//
//        WebUI.switchToWindowIndex('1')
//
//        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
//            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)
//
//            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)
//
//            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)
//
//            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgLockByOther'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
//
//                WebUI.delay(15)
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.delay(15)
//
//                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))
//
//                WebUI.delay(35)
//
//                WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else {
//                println('WBS or Construction Page')
//            }
//        } else {
//            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgLockByOther'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
//
//                WebUI.delay(15)
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
//                WebUI.delay(15)
//
//                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))
//
//                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))
//
//                WebUI.delay(35)
//
//                WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            } else {
//                println('WBS or Construction Page')
//
//                WebUI.closeWindowIndex('1')
//
//                WebUI.switchToWindowIndex('0')
//            }
//        }
//    }
//    
//    WebUI.comment('RFC Section')
//
//    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblPCRF'), 3, FailureHandling.OPTIONAL)) {
//        WebUI.mouseOver(findTestObject('Detail_ProjectID/lblPCRF'))
//
//        WebUI.click(findTestObject('Detail_ProjectID/lblPCRF'))
//
//        if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
//            List<WebElement> listPCRF = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 
//                30)
//
//            for (WebElement PCRF : listPCRF) {
//                PCRF.click()
//
//                WebUI.delay(1)
//            }
//        } else {
//            println('Document Persetujuan Not Found')
//        }
//        
//        if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 3, FailureHandling.OPTIONAL)) {
//            List<WebElement> listBRDUCS = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)
//
//            for (WebElement BRDUCS : listBRDUCS) {
//                BRDUCS.click()
//
//                WebUI.delay(2)
//
//                WebUI.switchToWindowIndex('1')
//
//                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
//                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)
//
//                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)
//
//                    WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)
//
//                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
//                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
//
//                        WebUI.closeWindowIndex('1')
//
//                        WebUI.switchToWindowIndex('0')
//
//                        WebUI.back()
//
//                        WebUI.delay(2)
//
//                        WebUI.click(findTestObject('btnTreeActions'))
//
//                        WebUI.click(findTestObject('lblExpandAll'))
//                    } else {
//                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))
//
//                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))
//
//                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
//
//                        WebUI.delay(25)
//
//                        WebUI.closeWindowIndex('1')
//
//                        WebUI.switchToWindowIndex('0')
//
//                        WebUI.back()
//
//                        WebUI.delay(2)
//
//                        WebUI.click(findTestObject('btnTreeActions'))
//
//                        WebUI.click(findTestObject('lblExpandAll'))
//                    }
//                } else {
//                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
//                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
//
//                        WebUI.closeWindowIndex('1')
//
//                        WebUI.switchToWindowIndex('0')
//
//                        WebUI.back()
//
//                        WebUI.delay(2)
//
//                        WebUI.click(findTestObject('btnTreeActions'))
//
//                        WebUI.click(findTestObject('lblExpandAll'))
//                    } else {
//                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))
//
//                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))
//
//                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
//
//                        WebUI.delay(15)
//
//                        WebUI.closeWindowIndex('1')
//
//                        WebUI.switchToWindowIndex('0')
//
//                        WebUI.back()
//
//                        WebUI.delay(2)
//
//                        WebUI.click(findTestObject('btnTreeActions'))
//
//                        WebUI.click(findTestObject('lblExpandAll'))
//                    }
//                }
//            }
//        } else {
//            println('Document BRD and UCS Not Found')
//
//            WebUI.back()
//
//            WebUI.delay(2)
//
//            WebUI.click(findTestObject('btnTreeActions'))
//
//            WebUI.click(findTestObject('lblExpandAll'))
//        }
//    } else {
//        println(' PCRF Not Found')
//    }
}

