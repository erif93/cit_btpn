import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Robot robot = new Robot()

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

WebUI.delay(5)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/abcd/lblJFMF-ParameterSettingCrossSellETHCI'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_020FPDK  FUDIP Online'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_027Enhancement Wholesale PMS Dashboard'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_038Enhancement OPICS - Penambahan Code untuk Penempatan NON BANK (PNB)'), 
    60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_049FES  Enhancement ECM'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_59IT PMO Dashboard'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_070Enhancement Edapem (LSUP  LRPP) Phase 2'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_081MIGRASI SWIFT ALLIANCE VERSION 72'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_95Managed File Transfer Platform (MFT)'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_106Enhancement CRMS phase 3'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_NCD New Paramenter'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_135BTPN Website Revamp'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_150MIGRASI APLIKASI SALES AGEN (BOBO)'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_164Enhancement View STG TPL Daily'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_183Interface SLIK'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_196Deaktivasi Rekening Nasabah di Bank Portal'), 
    60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/NEW_Checkpoint/span_212Enhancement Handling Transaksi Debit BCA dengan Aplet VISA di TWO'), 
    60)

WebUI.delay(5)

int a = 0

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('btnTestObjectGreenIcon'), 30)

for (WebElement link : mylist) {
    mylist.get(a).click()

    a++

    //RFC
    WebUI.comment('RFC')

    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblRFC'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> RFCList = WebUiCommonHelper.findWebElements(findTestObject('Mode1_Complete/lblDynamicRFCMode1Complete'), 
            30)

        c = 0

        sizelist = RFCList.size()

        for (WebElement RFC : RFCList) {
            WebUI.scrollToElement(findTestObject('Mode1_Complete/lblRFCMode1'), 60)

            WebElement context = RFCList.get(c)

            WebUI.delay(1)

            actions.moveToElement(context).perform()

            WebUI.delay(1)

            actions.contextClick(context).perform()

            WebUI.delay(1)

            c++

            robot.keyPress(KeyEvent.VK_DOWN)

            robot.keyRelease(KeyEvent.VK_DOWN)

            WebUI.delay(1)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            WebUI.delay(2)

            WebUI.switchToWindowIndex('1')

            WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

            WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

            WebUI.switchToWindowIndex('2')

            WebUI.delay(6)

            robot.keyPress(KeyEvent.VK_CONTROL)

            robot.keyPress(KeyEvent.VK_P)

            robot.keyRelease(KeyEvent.VK_P)

            robot.keyRelease(KeyEvent.VK_CONTROL)

            WebUI.delay(7)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            WebUI.delay(3)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyPress(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            robot.keyRelease(KeyEvent.VK_ENTER)

            WebUI.closeWindowIndex('2')

            WebUI.delay(1)

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            CustomKeywords.'clickJS.clickUsingJS'(findTestObject('divBlackBanner'), 60)
        }
    } else {
        println('RFC Not Found')
    }
}

