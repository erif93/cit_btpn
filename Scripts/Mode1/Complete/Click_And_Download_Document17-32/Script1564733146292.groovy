import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions

Robot robot = new Robot()

WebUI.delay(5)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/abcd/lblPengkinianTampilanAwalCustomerPortal'), 60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/abcd/lblEnhancementJFAST-FinancingCooperationwithOtherMultifinancewithnewfinancegoods(Phase1)'), 
    60)

WebUI.scrollToElement(findTestObject('Mode1_Complete/Checkpoint/Checkpoint Trial/ab/lblPenambahanProdukKRKMUR(code CC CG)'), 
    60)

WebUI.delay(5)

not_run: int i = 1

not_run: List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('lblProjectOtherCategory'), 30)

not_run: for (WebElement link : mylist) {
    link.click()

    WebUI.delay(5)

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.delay(4)

    WebUI.switchToWindowIndex('1')

    WebUI.delay(2)

    // Save Main Document
    WebUI.comment('Main Document Section ')

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(15)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(2)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    //Direct Document
    WebUI.comment('Download Direct Document Section')

    List<WebElement> listDocument = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 30)

    for (WebElement document : listDocument) {
        document.click()

        WebUI.delay(2)
    }
    
    // BRD, SIT , WBS , UCS , UAT ,FSD
    WebUI.comment(' BRD, WBS , SIT , UAT Section')

    List<WebElement> rlLink = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

    for (WebElement rLink : rlLink) {
        rLink.click()

        WebUI.delay(2)

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 3, FailureHandling.OPTIONAL)) {
            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0002)

            WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0002)

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(20)

                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(30)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else {
                println('WBS or Construction Page')

                WebUI.delay(10)

                CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(i + '_wbs_mode1.png', FailureHandling.STOP_ON_FAILURE)

                i++
            }
        } else {
            if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                WebUI.delay(20)

                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                WebUI.delay(30)

                WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                WebUI.delay(30)

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                    WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                }
                
                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')
            } else {
                println('WBS or Construction Page')

                WebUI.delay(15)

                CustomKeywords.'takeScreenShotFullPage.takeEntirePageScreenshot'(i + '_wbs_mode1.png', FailureHandling.STOP_ON_FAILURE)

                WebUI.closeWindowIndex('1')

                WebUI.switchToWindowIndex('0')

                i++
            }
        }
    }
    
    //PCRF 
    WebUI.comment('PCRF Section')

    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblPCRF'), 3, FailureHandling.OPTIONAL)) {
        WebUI.mouseOver(findTestObject('Detail_ProjectID/lblPCRF'))

        WebUI.delay(3)

        WebUI.rightClick(findTestObject('Detail_ProjectID/lblPCRF'))

        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        WebUI.switchToWindowIndex('1')

        WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

        WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

        WebUI.switchToWindowIndex('2')

        WebUI.delay(7)

        robot.keyPress(KeyEvent.VK_CONTROL)

        robot.keyPress(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_CONTROL)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        WebUI.closeWindowIndex('2')

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
            List<WebElement> listPCRF = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 
                30)

            for (WebElement PCRF : listPCRF) {
                PCRF.click()

                WebUI.delay(1)
            }
        } else {
            println('Document Persetujuan Not Found')
        }
        
        if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 3, FailureHandling.OPTIONAL)) {
            List<WebElement> listBRDUCS = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

            for (WebElement BRDUCS : listBRDUCS) {
                BRDUCS.click()

                WebUI.delay(2)

                WebUI.switchToWindowIndex('2')

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0002)

                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0002)

                    WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.closeWindowIndex('2')

                        WebUI.closeWindowIndex('1')

                        WebUI.switchToWindowIndex('0')
                    } else {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(25)

                        WebUI.closeWindowIndex('2')

                        WebUI.closeWindowIndex('1')

                        WebUI.switchToWindowIndex('0')
                    }
                } else {
                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.closeWindowIndex('2')

                        WebUI.closeWindowIndex('1')

                        WebUI.switchToWindowIndex('0')
                    } else {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(15)

                        WebUI.closeWindowIndex('2')

                        WebUI.closeWindowIndex('1')

                        WebUI.switchToWindowIndex('0')
                    }
                }
            }
        } else {
            println('Document BRD and UCS Not Found')

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')
        }
    } else {
        println(' PCRF Not Found')
    }
    
    //RFC 
    WebUI.comment('RFC')

    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/lblRFC'), 3, FailureHandling.OPTIONAL)) {
        WebUI.mouseOver(findTestObject('Detail_ProjectID/lblRFC'))

        WebUI.rightClick(findTestObject('Detail_ProjectID/lblRFC'))

        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)

        WebUI.delay(2)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(2)

        WebUI.switchToWindowIndex('1')

        WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

        WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

        WebUI.switchToWindowIndex('2')

        WebUI.delay(6)

        robot.keyPress(KeyEvent.VK_CONTROL)

        robot.keyPress(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_P)

        robot.keyRelease(KeyEvent.VK_CONTROL)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.delay(3)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyPress(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        robot.keyRelease(KeyEvent.VK_ENTER)

        WebUI.closeWindowIndex('2')

        WebUI.delay(1)

        WebUI.switchToWindowIndex('1')

        if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
            List<WebElement> listRFC = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 30)

            for (WebElement RFC : listRFC) {
                RFC.click()

                WebUI.delay(1)
            }
        } else {
            println('Document Persetujuan Not Found')
        }
        
        if (WebUI.verifyElementPresent(findTestObject('hrefrlLink'), 3, FailureHandling.OPTIONAL)) {
            List<WebElement> listImplantSITUAT = WebUiCommonHelper.findWebElements(findTestObject('hrefrlLink'), 30)

            for (WebElement implantSITUAT : listImplantSITUAT) {
                implantSITUAT.click()

                WebUI.delay(2)

                WebUI.switchToWindowIndex('2')

                if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPointCIT0002)

                    WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPointCIT0002)

                    WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.delay(3)

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(20)

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.delay(15)

                        WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                        WebUI.delay(25)

                        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                            WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                        }
                        
                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else {
                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    }
                } else {
                    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

                        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

                        WebUI.delay(15)

                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgPDFIcon'), 3, FailureHandling.OPTIONAL)) {
                        WebUI.delay(15)

                        WebUI.click(findTestObject('Detail_ProjectID/imgPDFIcon'))

                        WebUI.click(findTestObject('Detail_ProjectID/lblExportDetails'))

                        WebUI.delay(25)

                        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefTableCellLink'), 3, FailureHandling.OPTIONAL)) {
                            WebUI.click(findTestObject('Detail_ProjectID/hrefTableCellLink'))
                        }
                        
                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    } else {
                        WebUI.closeWindowIndex('2')

                        WebUI.switchToWindowIndex('1')
                    }
                }
            }
            
            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            WebUI.click(findTestObject('Mode1_Complete/lblContinuousIntegrationToolsAllProject'))
        } else {
            println('Document SIT/UAT Not Found')

            WebUI.delay(2)

            WebUI.closeWindowIndex('1')

            WebUI.switchToWindowIndex('0')

            WebUI.click(findTestObject('Mode1_Complete/lblContinuousIntegrationToolsAllProject'))
        }
    } else {
        println(' RFC Not Found')
    }
}

