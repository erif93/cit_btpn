import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(2)

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblpulsamu'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblCustomerDataInfoStaging'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblMISPortal'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblProxy'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblHelpdeskSystemforBTPNexecutivesincludingITServiceCatalogueServiceLevelManagement'), 
    60)

WebUI.delay(5)

List<WebElement> mode0List = WebUiCommonHelper.findWebElements(findTestObject('lblProjectOtherCategory'), 30)

i = 1

for (WebElement mode0 : mode0List) {
    mode0.click()

    WebUI.mouseOver(findTestObject('Detail_ProjectID/lblStatus'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Detail_ProjectID/lblStatus'))

    WebUI.delay(7)

    CustomKeywords.'takeScreenShotFullPage.takeWebElementScreenshot'(findTestObject('Mode0Complete/divFpHistoryDialog'), 
        i + '_logchanges_mode1_OtherCategory.png', 60, FailureHandling.STOP_ON_FAILURE)

    i++

    WebUI.click(findTestObject('Mode0Complete/span_Close'))

    WebUI.delay(2)
}

