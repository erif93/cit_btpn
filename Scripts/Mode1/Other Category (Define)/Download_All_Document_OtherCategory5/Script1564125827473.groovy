import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblpulsamu'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblCustomerDataInfoStaging'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblMISPortal'), 60)

WebUI.scrollToElement(findTestObject('Mode_1_OtherCategory/Checkpoint_Element/lblProxy'), 60)

WebUI.delay(5)

List<WebElement> otherList = WebUiCommonHelper.findWebElements(findTestObject('lblProjectOtherCategory'), 30)

for (WebElement otherCategory : otherList) {
    otherCategory.click()

    WebUI.delay(5)

    WebUI.mouseOver(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.delay(3)

    WebUI.switchToWindowIndex('1')

    WebUI.delay(5)

    // Save Main Document
    WebUI.comment('Main Document Section ')

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(15)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(3)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    //Direct Document
    WebUI.comment('Attachment Section')

    if (WebUI.verifyElementPresent(findTestObject('hrefMainDokumenPersetujuan'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listDocument = WebUiCommonHelper.findWebElements(findTestObject('hrefMainDokumenPersetujuan'), 
            30)

        for (WebElement document : listDocument) {
            if (WebUI.verifyElementNotPresent(findTestObject('Mode1_Reject/hrefCapturePNG'), 2, FailureHandling.OPTIONAL) && 
            WebUI.verifyElementNotPresent(findTestObject('Mode1_Reject/hrefCaptureJPG'), 2, FailureHandling.OPTIONAL)) {
                document.click()

                WebUI.delay(2)
            } else if (WebUI.verifyElementPresent(findTestObject('Mode1_Reject/hrefCapturePNG'), 2, FailureHandling.OPTIONAL)) {
                println('PNG Document')
            } else if (WebUI.verifyElementPresent(findTestObject('Mode1_Reject/hrefCaptureJPG'), 2, FailureHandling.OPTIONAL)) {
                println('JPG Document')
            }
        }
    } else {
        println('Document Not Found')
    }
}

