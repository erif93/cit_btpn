import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys

Robot robot = new Robot()

WebUI.delay(3)

WebUI.click(findTestObject('btnTreeActions'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('Infranet/On_Progress/hrefCollapseAll'))

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.scrollToElement(findTestObject('Mode0Complete/span_Upgrade DWDM'), 60)

WebUI.scrollToElement(findTestObject('Mode0Complete/span_Penambahan Access Point WiFi Lantai 17 (Wow)'), 60)

WebUI.delay(5)

List<WebElement> mode0List = WebUiCommonHelper.findWebElements(findTestObject('lblProjectOtherCategory'), 30)

for (WebElement mode0 : mode0List) {
    mode0.click()

    WebUI.delay(5)

    WebUI.mouseOver(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('ProjectID1-10/hrefActions'))

    WebUI.click(findTestObject('MainPage_Mode1Complete/btnDisplayPrinterFriendly'))

    WebUI.delay(3)

    WebUI.switchToWindowIndex('1')

    WebUI.delay(5)

    // Save Main Document
    WebUI.comment('Main Document Section ')

    robot.keyPress(KeyEvent.VK_CONTROL)

    robot.keyPress(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_P)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    WebUI.delay(6)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(3)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')

    //Direct Document
    WebUI.comment('Attachment Section')

    if (WebUI.verifyElementPresent(findTestObject('Extension/lblPDFExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listPDF = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblPDFExtension'), 30)

        for (WebElement documentPDF : listPDF) {
            documentPDF.click()
        }
    }
    
    if (WebUI.verifyElementPresent(findTestObject('Extension/lblXlsExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listXLS = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblXlsExtension'), 30)

        for (WebElement documentXLS : listXLS) {
            documentXLS.click()
        }
    }
    
    if (WebUI.verifyElementPresent(findTestObject('Extension/lblPPTExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listPPT = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblPPTExtension'), 30)

        for (WebElement documentPPT : listPPT) {
            documentPPT.click()
        }
    }
    
    if (WebUI.verifyElementPresent(findTestObject('Extension/lblPPTExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listPPT = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblPPTExtension'), 30)

        for (WebElement documentPPT : listPPT) {
            documentPPT.click()
        }
    }
    
    if (WebUI.verifyElementPresent(findTestObject('Extension/lblDocExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listDOC = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblDocExtension'), 30)

        for (WebElement documentDOC : listDOC) {
            documentDOC.click()
        }
    }
    
    if (WebUI.verifyElementPresent(findTestObject('Extension/lblMsgExtension'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> listMSG = WebUiCommonHelper.findWebElements(findTestObject('Extension/lblMsgExtension'), 30)

        for (WebElement documentMSG : listMSG) {
            documentMSG.click()
        }
    }
}

