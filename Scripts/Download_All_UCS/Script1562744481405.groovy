import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.click(findTestObject('ProjectID1-10/lblProjectID001'))

WebUI.delay(30)

List<WebElement> mylist = WebUiCommonHelper.findWebElements(findTestObject('testLinkObject'), 30)

for (WebElement link : mylist) {
    link.click()

    WebUI.delay(2)

    WebUI.switchToWindowIndex('1')

    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), 0, FailureHandling.OPTIONAL)) {
        WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpUsername'), GlobalVariable.usernameFocalPoint)

        WebUI.setText(findTestObject('Detail_ProjectID/txtboxPopUpPassword'), GlobalVariable.passwordFocalPoint)

        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpLogin'), FailureHandling.STOP_ON_FAILURE)

        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 0, FailureHandling.OPTIONAL)) {
            WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
        } else {
            WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

            WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
			
			WebUI.delay(15)
			
			WebUI.closeWindowIndex('1')
			
		    WebUI.switchToWindowIndex('0')
        }
    } else {
        if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 0, FailureHandling.OPTIONAL)) {
            WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
        } else {
            WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

            WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

            WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))
        }
    }
    
    WebUI.delay(15)

    WebUI.closeWindowIndex('1')

    WebUI.switchToWindowIndex('0')
}


