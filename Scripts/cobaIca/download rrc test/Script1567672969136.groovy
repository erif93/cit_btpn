import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()

Actions actions = new Actions(driver)

Robot robot = new Robot()

WebUI.delay(5)
if (WebUI.verifyElementPresent(findTestObject('linkDoc'), 3, FailureHandling.OPTIONAL)) {
        List<WebElement> docList = WebUiCommonHelper.findWebElements(findTestObject('linkDoc'), 30)

        
        int d = 0

        sizeDoc = docList.size()

        for (WebElement docMain : docList) {
            WebElement contextDoc = docList.get(d)

            actions.moveToElement(contextDoc).perform()

            actions.contextClick(contextDoc).perform()

            WebUI.delay(2)

            d++
			
        robot.keyPress(KeyEvent.VK_DOWN)

        robot.keyRelease(KeyEvent.VK_DOWN)
		
		WebUI.delay(1)

        robot.keyPress(KeyEvent.VK_RIGHT)

        robot.keyRelease(KeyEvent.VK_RIGHT)
		
		WebUI.delay(1)
		
		robot.keyPress(KeyEvent.VK_DOWN)

		robot.keyRelease(KeyEvent.VK_DOWN)
		
		WebUI.delay(1)
		
		robot.keyPress(KeyEvent.VK_DOWN)

		robot.keyRelease(KeyEvent.VK_DOWN)
		
		WebUI.delay(1)

		robot.keyPress(KeyEvent.VK_ENTER)
		
		robot.keyRelease(KeyEvent.VK_ENTER)
				
        WebUI.delay(3)

        WebUI.switchToWindowIndex('1')


    
		WebUI.delay(2)
    if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'), 3, FailureHandling.OPTIONAL)) {
        WebUI.click(findTestObject('Detail_ProjectID/hrefDetailDownloadForViewing'))
		WebUI.delay(1)
		WebUI.closeWindowIndex('1')
		WebUI.switchToWindowIndex('0')
		
       
    } else if (WebUI.verifyElementPresent(findTestObject('Detail_ProjectID/imgMoreActions'), 3, FailureHandling.OPTIONAL)) {
        WebUI.click(findTestObject('Detail_ProjectID/imgMoreActions'))

        WebUI.click(findTestObject('Detail_ProjectID/lblCreateAndPrintDocumentPDF'))

        WebUI.click(findTestObject('Detail_ProjectID/btnPopUpOk'))

        WebUI.delay(10)
		WebUI.closeWindowIndex('1')
        WebUI.switchToWindowIndex('0')
		
    } else {
        println('WBS or Construction Page')


    }
        }
}


