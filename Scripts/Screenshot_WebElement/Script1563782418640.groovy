import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('btnTreeActions'))

WebUI.click(findTestObject('lblExpandAll'))

WebUI.click(findTestObject('ProjectID1-10/lblProjectIdExpertCoaching'))

WebUI.delay(7)

WebUI.mouseOver(findTestObject('Detail_ProjectID/lblStatus'))

WebUI.click(findTestObject('Detail_ProjectID/lblStatus'))

WebUI.mouseOver(findTestObject('Detail_ProjectID/a_Logged Changes'))

WebUI.click(findTestObject('Detail_ProjectID/a_Logged Changes'))

WebUI.delay(12)

CustomKeywords.'takeScreenShotFullPage.takeWebElementScreenshot'(findTestObject('Detail_ProjectID/divStatusHistory'), 'testya.png', 
    60, FailureHandling.STOP_ON_FAILURE)

