<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divStatusHistoryDialog</name>
   <tag></tag>
   <elementGuidId>3661b4ef-86fe-4880-8df6-be79f7a51c8f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>idxDialog idxDialogHover dijitHover idxDialogFocused idxDialogHoverFocused dijitHoverFocused dijitFocused</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>fpHistoryDialog_title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>fpHistoryDialog_desc</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fpHistoryDialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en-us</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>fpHistoryDialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Status History for EQUATION 4.0






    



    
        
	        
                
                	       
	            All Attributes
	        
                
                
                    
                	       
	            ID
	        
                
                
                    
                	       
	            Project ID
	        
                
                
                    
                	       
	            Skala Project
	        
                
                
                    
                	       
	            No. PRF
	        
                
                
                    
                	       
	            Nama Proyek
	        
                
                
                    
                	       
	            Deskripsi Proyek
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap PRF
	        
                
                
                    
                	       
	            Ruang Lingkup
	        
                
                
                    
                	       
	            Latar Belakang Proyek
	        
                
                
                    
                	       
	             
	        
                
                
                    
                	       
	            Kategori Urgensi
	        
                
                
                    
                	       
	            Kategori Proyek
	        
                
                
                    
                	       
	            Kategori Proyek
	        
                
                
                    
                	       
	            Kategori Proses
	        
                
                
                    
                	       
	            Type Project
	        
                
                
                    
                	       
	            Nama Vendor
	        
                
                
                    
                	       
	            Related Application
	        
                
                
                    
                	       
	            Application Owner
	        
                
                
                    
                	       
	            Status
	        
                
                
                    
                	       
	            Mulai
	        
                
                
                    
                	       
	            Perkiraan Selesai
	        
                
                
                    
                	       
	            Penyelesaian BRD
	        
                
                
                    
                	       
	            Penyelesaian Project Charter
	        
                
                
                    
                	       
	            Implikasi terhadap Sistem Lain
	        
                
                
                    
                	       
	            User Yang Terlibat
	        
                
                
                    
                	       
	            Kondisi Saat Ini 
	        
                
                
                    
                	       
	            Kondisi yang Diharapkan 
	        
                
                
                    
                	       
	            Manfaat Proyek 
	        
                
                
                    
                	       
	            Deliverables Proyek
	        
                
                
                    
                	       
	            Implikasi terhadap Sistem Lain
	        
                
                
                    
                	       
	            User Yang Terlibat
	        
                
                
                    
                	       
	            Kondisi Saat Ini
	        
                
                
                    
                	       
	            Kondisi yang Diharapkan
	        
                
                
                    
                	       
	            Manfaat Proyek
	        
                
                
                    
                	       
	            Deliverables Proyek
	        
                
                
                    
                	       
	            Budget ID
	        
                
                
                    
                	       
	            Nilai Rupiah Terhadap USD
	        
                
                
                    
                	       
	            Usulan - Hardware Server
	        
                
                
                    
                	       
	            Usulan - Hardware Non Server
	        
                
                
                    
                	       
	            Usulan - Software / Biaya Pengembangan
	        
                
                
                    
                	       
	            Usulan - Opex / Professional Service
	        
                
                
                    
                	       
	            Biaya Disetujui
	        
                
                
                    
                	       
	            Biaya Disetujui
	        
                
                
                    
                	       
	            Dokumen Pendukung Biaya Disetujui
	        
                
                
                    
                	       
	            Actual Cost
	        
                
                
                    
                	       
	            BRD
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap BRD
	        
                
                
                    
                	       
	            Use Case Specification
	        
                
                
                    
                	       
	            Need Risk
	        
                
                
                    
                	       
	            WBS
	        
                
                
                    
                	       
	            Target Go Live
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap PCH
	        
                
                
                    
                	       
	            Pendahuluan
	        
                
                
                    
                	       
	            2.1 Kebutuhan Bisnis
	        
                
                
                    
                	       
	            2.2 Tujuan
	        
                
                
                    
                	       
	            3.1 Hasil Akhir
	        
                
                
                    
                	       
	            3.2 Kriteria Keberhasilan Proyek
	        
                
                
                    
                	       
	            4.1 Ruang Lingkup - Dalam Lingkup Project
	        
                
                
                    
                	       
	            4.2 Ruang Lingkup - Luar Lingkup Project
	        
                
                
                    
                	       
	            4.3 Budget
	        
                
                
                    
                	       
	            4.4 Jadwal
	        
                
                
                    
                	       
	            4.5 Kebergantungan Dengan Project Lain
	        
                
                
                    
                	       
	            4.6 Batasan Lainnya
	        
                
                
                    
                	       
	            5.1 Struktur Organisasi Proyek
	        
                
                
                    
                	       
	            FSD
	        
                
                
                    
                	       
	            Activity Constructions
	        
                
                
                    
                	       
	            Implementation Project URI
	        
                
                
                    
                	       
	            Completeness
	        
                
                
                    
                	       
	            Last Snapshot Date
	        
                
                
                    
                	       
	            Test Plan
	        
                
                
                    
                	       
	            Nama Component dan Baseline
	        
                
                
                    
                	       
	            Test Plan
	        
                
                
                    
                	       
	            Nama Component dan Baseline
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap UAT
	        
                
                
                    
                	       
	            Result VA and PT
	        
                
                
                    
                	       
	            RFC
	        
                
                
                    
                	       
	            Anggaran Disetujui
	        
                
                
                    
                	       
	            Aktual - Hardware Server 
	        
                
                
                    
                	       
	            Aktual - Hardware Non Server 
	        
                
                
                    
                	       
	            Aktual - Software / Biaya Pengembangan 
	        
                
                
                    
                	       
	            Aktual - Opex / Professional Service 
	        
                
                
                    
                	       
	            Aktual Anggaran
	        
                
                
                    
                	       
	            Dokumen Pendukung Biaya Aktual
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap PCL
	        
                
                
                    
                	       
	            Attach Dokumen Post Implementation Readiness Review (PIRR)
	        
                
                
                    
                	       
	            1.1 Timeline Evaluation
	        
                
                
                    
                	       
	            1.2 Scope Of Work Evaluation
	        
                
                
                    
                	       
	            Anggaran Disetujui
	        
                
                
                    
                	       
	            Aktual - Hardware Server
	        
                
                
                    
                	       
	            Aktual - Hardware Non Server
	        
                
                
                    
                	       
	            Aktual - Software / Biaya Pengembangan
	        
                
                
                    
                	       
	            Aktual - Opex / Professional Service
	        
                
                
                    
                	       
	            Evaluasi Anggaran
	        
                
                
                    
                	       
	            Aktual Anggaran
	        
                
                
                    
                	       
	            Dokumen Pendukung Biaya Aktual
	        
                
                
                    
                	       
	            Pelajaran Yang Didapat
	        
                
                
                    
                	       
	            IT Project Manager Unit Head
	        
                
                
                    
                	       
	            IT Project Manager Head
	        
                
                
                    
                	       
	            IT Planning Head
	        
                
                
                    
                	       
	            Status PRF Approve
	        
                
                
                    
                	       
	            IT Strategy &amp; Planning Head
	        
                
                
                    
                	       
	            IT Vendor Management Head
	        
                
                
                    
                	       
	            Status Requirement Definition Review 
	        
                
                
                    
                	       
	            IT Business Alliance Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT Business Alliance Head
	        
                
                
                    
                	       
	            IT Busol Head
	        
                
                
                    
                	       
	            Status WBS Approver
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT SQA Unit Head
	        
                
                
                    
                	       
	            IT Business Alliance Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT PMO Head
	        
                
                
                    
                	       
	            Status PCH Review
	        
                
                
                    
                	       
	            IT Project Manager Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT PMO Head
	        
                
                
                    
                	       
	            Status PCH Approve
	        
                
                
                    
                	       
	            IT Project Manager Head
	        
                
                
                    
                	       
	            IT Strategy &amp; Planning Head
	        
                
                
                    
                	       
	            Status Design Review
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT Infra Dev Head
	        
                
                
                    
                	       
	            IT Sol Dev Head
	        
                
                
                    
                	       
	            IT Busol Head
	        
                
                
                    
                	       
	            Status Development Approve 
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT SQA Unit Head
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            Status UAT Review
	        
                
                
                    
                	       
	            IT Business Alliance Unit Head
	        
                
                
                    
                	       
	            IT Prod Supp Head
	        
                
                
                    
                	       
	            IT SQA Unit Head
	        
                
                
                    
                	       
	            IT Imp Supp Head
	        
                
                
                    
                	       
	            IT Business Alliance Head
	        
                
                
                    
                	       
	            Status Implementation Plan Review 
	        
                
                
                    
                	       
	            Status Implementation Plan Approve
	        
                
                
                    
                	       
	            IT Busol Unit Head
	        
                
                
                    
                	       
	            IT Sol Dev Unit Head
	        
                
                
                    
                	       
	            IT Infra Dev Head
	        
                
                
                    
                	       
	            IT Service Support Head
	        
                
                
                    
                	       
	            IT Data Operations Unit Head
	        
                
                
                    
                	       
	            Status Implementation Plan Final Approve
	        
                
                
                    
                	       
	            IT Imp Supp Unit Head
	        
                
                
                    
                	       
	            IT Prod Supp Head
	        
                
                
                    
                	       
	            IT Infrastructure Operations Head
	        
                
                
                    
                	       
	            IT Project Manager Unit Head
	        
                
                
                    
                	       
	            IT PMO Head
	        
                
                
                    
                	       
	            IT SQA Unit Head
	        
                
                
                    
                	       
	            IT Business Alliance Head
	        
                
                
                    
                	       
	            No. Tiket Incident
	        
                
                
                    
                	       
	            Additional Attachment
	        
                
                
                    
                	       
	            Purchase Order
	        
                
                
                    
                	       
	            VA Static Test
	        
                
                
                    
                	       
	            VA Dynamic Test
	        
                
                
                    
                	       
	            IT Project Manager
	        
                
                
                    
                	       
	            IT Business Alliance
	        
                
                
                    
                	       
	            IT SQA Leader
	        
                
                
                    
                	       
	            IT Busol Staf
	        
                
                
                    
                	       
	            IT Sol Dev Leader
	        
                
                
                    
                	       
	            IT Prod Supp Leader
	        
                
                
                    
                	       
	            IT Imp Supp Leader
	        
                
                
                    
                	       
	            Dokumen Override
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Dokumen Pendukung
	        
                
                
                    
                	       
	            Comments
	        
                
                
                    
                	       
	            Project RTC
	        
                
                
                    
                	       
	            Implementation Project URI
	        
                
                
                    
                	       
	            PRF Approve Link
	        
                
                
                    
                	       
	            PCH Review Link
	        
                
                
                    
                	       
	            Design Approve Link
	        
                
                
                    
                	       
	            System Test Review Link
	        
                
                
                    
                	       
	            RFC Approve Link
	        
                
                
                    
                	       
	            Evaluasi Waktu Pelaksanaan Link
	        
                
                
                    
                	       
	            Evaluasi Pengujian Produk Link
	        
                
                
                    
                	       
	            Request For Change Link
	        
                
                
                    
                	       
	            GUID
	        
                
                
                    
                	       
	            Rank
	        
                
                
                    
                	       
	            SetChoice
	        
                
                
                    
                	       
	            Admin 1
	        
                
                
                    
                	       
	            Admin 2
	        
        
    
    






    


        
            
                
                    
                        
                            Status History for EQUATION 4.0
                        
                    
                
                
                    
                    
                    
                    Member
                    Date
                    Changed To
                    
                         
                    
                
            
            
                
                    
                        
                        
                        
                        Element added by Admin.
                        9/16/2015 6:33 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				Project Complete
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
            
            
                
                    
                        Status in EQUATION 4.0 has changed once at 9/16/2015 6:33 PM.
                    
                
            
        
		

NO LINK●Close</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fpHistoryDialog&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='fpHistoryDialog']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[319]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[4]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[21]</value>
   </webElementXpaths>
</WebElementEntity>
