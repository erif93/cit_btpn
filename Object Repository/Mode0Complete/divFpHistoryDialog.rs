<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divFpHistoryDialog</name>
   <tag></tag>
   <elementGuidId>9488873d-a4a4-4a31-a73e-18c3804c6ef3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>idxDialog idxDialogHover dijitHover idxDialogFocused idxDialogHoverFocused dijitHoverFocused dijitFocused</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>fpHistoryDialog_title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>fpHistoryDialog_desc</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fpHistoryDialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en-us</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>fpHistoryDialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Status History for Kerjasama BTPN - Kredit Biro Swasta (KBS)






    



    
        
	        
                
                	       
	            All Attributes
	        
                
                
                    
                	       
	            ID
	        
                
                
                    
                	       
	            Project ID
	        
                
                
                    
                	       
	            Skala Project
	        
                
                
                    
                	       
	            No. PRF
	        
                
                
                    
                	       
	            Nama Proyek
	        
                
                
                    
                	       
	            Deskripsi Proyek
	        
                
                
                    
                	       
	            Attach Dokumen Persetujuan User Tahap PRF
	        
                
                
                    
                	       
	            Ruang Lingkup
	        
                
                
                    
                	       
	            Latar Belakang Proyek
	        
                
                
                    
                	       
	             
	        
                
                
                    
                	       
	            Kategori Urgensi
	        
                
                
                    
                	       
	            Kategori Proyek
	        
                
                
                    
                	       
	            Kategori Proses
	        
                
                
                    
                	       
	            Type Project
	        
                
                
                    
                	       
	            Nama Vendor
	        
                
                
                    
                	       
	            Related Application
	        
                
                
                    
                	       
	            Application Owner
	        
                
                
                    
                	       
	            Status
	        
                
                
                    
                	       
	            Mulai
	        
                
                
                    
                	       
	            Perkiraan Selesai
	        
                
                
                    
                	       
	            Implikasi terhadap Sistem Lain
	        
                
                
                    
                	       
	            User Yang Terlibat
	        
                
                
                    
                	       
	            Kondisi Saat Ini 
	        
                
                
                    
                	       
	            Kondisi yang Diharapkan 
	        
                
                
                    
                	       
	            Manfaat Proyek 
	        
                
                
                    
                	       
	            Deliverables Proyek
	        
                
                
                    
                	       
	            Usulan - Hardware Server
	        
                
                
                    
                	       
	            Usulan - Hardware Non Server
	        
                
                
                    
                	       
	            Usulan - Software / Biaya Pengembangan
	        
                
                
                    
                	       
	            Usulan - Opex / Professional Service
	        
                
                
                    
                	       
	            Biaya Disetujui
	        
                
                
                    
                	       
	            Dokumen Pendukung Biaya Disetujui
	        
                
                
                    
                	       
	            IT Project Manager
	        
                
                
                    
                	       
	            Dokumen Override
	        
        
    
    






    


        
            
                
                    
                        
                            Status History for Kerjasama BTPN - Kredit Biro Swasta (KBS)
                        
                    
                
                
                    
                    
                    
                    Member
                    Date
                    Changed To
                    
                         
                    
                
            
            
                
                    
                        
                        
                        
                        Yenny Setiawati
                        7/21/2017 1:54 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				Project Complete
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
                    
                        
                        
                        
                        Daud Lesmana
                        5/30/2017 3:28 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				CB-01_Evaluasi Penggunaan Anggaran
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
                    
                        
                        
                        
                        Stephanus HM Latuputty
                        3/24/2017 7:29 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				BAST-01_Attach BAST Doc
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
                    
                        
                        
                        
                        Diah Octaviani Kusnomo
                        2/1/2017 8:10 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				PRF-02_Waiting PO
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
                    
                        
                        
                        
                        Daud Lesmana
                        2/1/2017 7:57 AM
                        
                             
                            




	
		 
		     
			
				
			 
			
				PRF-01_PRF Approve
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
                    
                        
                        
                        
                        Element added by Daud Lesmana.
                        1/31/2017 5:39 PM
                        
                             
                            




	
		 
		     
			
				
			 
			
				PRF-00_Draft PRF
			
		
	


                        
                        
                            
                                
                            
                        
                    
                
            
            
                
                    
                        Status in Kerjasama BTPN - Kredit Biro Swasta (KBS) has changed 6 times between 7/21/2017 1:54 PM and 1/31/2017 5:39 PM.
                    
                
            
        
		

NO LINK●Close</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fpHistoryDialog&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='fpHistoryDialog']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[43]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[4]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[21]</value>
   </webElementXpaths>
</WebElementEntity>
