<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblProjectID001</name>
   <tag></tag>
   <elementGuidId>27007f81-7143-41af-881c-86817fbb8f91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='fp-tree-container']/ul/li[2]/ul/li/ul/li/a/span[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;fp-tree-container&quot;)/ul[@class=&quot;fpTree&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeElement&quot;]/a[@class=&quot;fpTreeNodeLabel fpTreeNodeLabelSelected&quot;]/span[@class=&quot;fpTreeNodeItemIcon fpIconDyn fpOkIcon fp-treenode-nonfolder&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fpTreeNodeItemLabel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Proteksi GL Account SC124 - Sys Gen Posting Only pada Saat Pembukaan Cabang Baru</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fp-tree-container&quot;)/ul[@class=&quot;fpTree&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeElement&quot;]/a[@class=&quot;fpTreeNodeLabel fpTreeNodeLabelSelected&quot;]/span[@class=&quot;fpTreeNodeItemLabel&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fp-tree-container']/ul/li[2]/ul/li/ul/li/a/span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elements (285|284)'])[1]/following::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='▼'])[1]/following::span[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Risk Datamart Phase 2'])[1]/preceding::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expert Coaching Application'])[1]/preceding::span[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li/ul/li/a/span[2]</value>
   </webElementXpaths>
</WebElementEntity>
