<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hrefAC</name>
   <tag></tag>
   <elementGuidId>3d4938d6-eb9e-4521-9e82-edd25b78c755</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[21]/div[2]/table/tbody/tr/td[3]/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>rlLink_81_13079_350_1142046732_937</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://appcitmpr01.corp.bankbtpn.co.id:9445/ccm/resource/itemOid/com.ibm.team.apt.IterationPlanRecord/_e-ARAKclEee0CYQwd7FM7A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>rlLink_81_13079_350_1142046732_937</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>https://appcitmpr01.corp.bankbtpn.co.id:9445/ccm/resource/itemOid/com.ibm.team.apt.IterationPlanRecord/_e-ARAKclEee0CYQwd7FM7A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rlLink_81_13079_350_1142046732_937&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='rlLink_81_13079_350_1142046732_937']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v_81_13079_350']/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'https://appcitmpr01.corp.bankbtpn.co.id:9445/ccm/resource/itemOid/com.ibm.team.apt.IterationPlanRecord/_e-ARAKclEee0CYQwd7FM7A')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[78]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activity Constructions'])[2]/following::a[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[36]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://appcitmpr01.corp.bankbtpn.co.id:9445/ccm/resource/itemOid/com.ibm.team.apt.IterationPlanRecord/_e-ARAKclEee0CYQwd7FM7A')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[21]/div[2]/table/tbody/tr/td[3]/div/div/a</value>
   </webElementXpaths>
</WebElementEntity>
