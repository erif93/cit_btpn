<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP2 span_1093CRF01Sonik Enhancement - User Mapping  ServicesUpgrade Version OS DB dan PHP SONIK</name>
   <tag></tag>
   <elementGuidId>3a503c8b-e1c8-42b7-946f-bd573f064855</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fpTreeNodeItemLabel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1093.CRF.01.Sonik Enhancement - User Mapping &amp; Services.Upgrade Version OS, DB. dan PHP SONIK</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fp-tree-container&quot;)/ul[@class=&quot;fpTree&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder&quot;]/ul[@class=&quot;fpTreeChildren  1&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeElement&quot;]/a[@class=&quot;fpTreeNodeLabel fpTreeNodeLabelSelected&quot;]/span[@class=&quot;fpTreeNodeItemLabel&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fp-tree-container']/ul/li[2]/ul/li[6]/ul/li/a/span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='▼'])[1]/following::span[24]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tree Actions'])[1]/following::span[26]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go to'])[1]/preceding::span[71]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='▼'])[2]/preceding::span[72]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li/a/span[2]</value>
   </webElementXpaths>
</WebElementEntity>
