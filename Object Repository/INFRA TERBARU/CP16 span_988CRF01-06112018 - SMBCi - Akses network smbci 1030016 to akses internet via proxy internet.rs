<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP16 span_988CRF01-06112018 - SMBCi - Akses network smbci 1030016 to akses internet via proxy internet</name>
   <tag></tag>
   <elementGuidId>d7e2c67e-f799-40f3-b0f1-6f2b71e32875</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fpTreeNodeItemLabel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>988.CRF.01.-.06112018 - SMBCi - Akses network smbci 10.3.0.0/16 to akses internet via proxy internet</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fp-tree-container&quot;)/ul[@class=&quot;fpTree&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder&quot;]/ul[@class=&quot;fpTreeChildren  1&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeFolder fpTreeNodeFolderOpen&quot;]/ul[@class=&quot;fpTreeChildren&quot;]/li[@class=&quot;fpTreeNode fpTreeNodeElement&quot;]/a[@class=&quot;fpTreeNodeLabel&quot;]/span[@class=&quot;fpTreeNodeItemLabel&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fp-tree-container']/ul/li[2]/ul/li[17]/ul/li/a/span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='▼'])[1]/following::span[66]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tree Actions'])[1]/following::span[68]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add and Delete Pool Member FSCM on F5 DMZ DRC (0|1)'])[1]/preceding::span[22]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add and Delete Pool Member FSCM on F5 DMZ DRC'])[1]/preceding::span[24]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[17]/ul/li/a/span[2]</value>
   </webElementXpaths>
</WebElementEntity>
