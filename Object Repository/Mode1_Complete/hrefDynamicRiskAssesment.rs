<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hrefDynamicRiskAssesment</name>
   <tag></tag>
   <elementGuidId>5e092d9c-eddb-4c0f-b761-c95ecfdac32d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[10]/div[2]/table/tbody/tr[2]/td[3]/div/div/table/tbody/tr/td/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fp-attrcell-81_15416_2237</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fpCell</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attrtype</name>
      <type>Main</type>
      <value>Text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attrid</name>
      <type>Main</type>
      <value>81_15416_2237</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attrurl</name>
      <type>Main</type>
      <value>/fp/workspace/81/view/429/element/15416/attribute/2237</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-type</name>
      <type>Main</type>
      <value>textfield</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-skey</name>
      <type>Main</type>
      <value>1D9CFC8E22D436A4623E9C3D02F04F83</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-headingid</name>
      <type>Main</type>
      <value>81_15416_2234</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-editable</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attr-informula</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attrname</name>
      <type>Main</type>
      <value>Risk Assessment</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
	        
		        
			        
			            
			            
			                





    

Risks1. Enhancement DHIB-SKNPokok PembahasanHak AksesPotensi ResikoAdanya akses tidak berkepentingan terhadap data kritikal.Rekomendasi Mitigasi Risiko- Pastikan hak akses sesuai dengan tanggung jawabnya- Pastikan tidak ada overlapping access privilege dan menu antara checker dan makerTanggapan Unit Kerja Pemohon LikelihoodImpactImpact DescriptionInformation ConfidentialityInformation Integrity (accuracy/ completeness)RGM RiskModerate to High2. Enhancement DHIB-SKNPokok PembahasanLevelling/Pengadaan DRPotensi Resiko- Potensi perbedaan data antara aplikasi di DC dan di DR.- Kebutuhan akan akses system pada saat terjadinya disaster.Rekomendasi Mitigasi Risiko•Agar berkonsultasi dengan IT System Continuation terkait proses pengadaan DR.Tanggapan Unit Kerja Pemohon LikelihoodImpactImpact DescriptionInformation Integrity (accuracy/ completeness)IT -  AvailabilityRGM RiskModerate3. Enhancement DHIB-SKNPokok PembahasanVA TestPotensi ResikoAdanya celah kelemahan keamanan yang mungkin terdapat dalam desain, implementasi atau pengunaan aplikasi.Rekomendasi Mitigasi RisikoAgar berkordinasi dengan IT Security terkait palaksanaan VA Test dan pastikan untuk di follow up hasil temuan VA.Tanggapan Unit Kerja Pemohon LikelihoodImpactImpact DescriptionInformation ConfidentialityRGM RiskModerate to High
    










Edit





			            
			        
		        
		        
	        
        
        
     </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fp-attrcell-81_15416_2237&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='fp-attrcell-81_15416_2237']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='fp-attribute-row-81_15416_2237']/td[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[48]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Risk Assessment'])[4]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div[2]/table/tbody/tr[2]/td[3]</value>
   </webElementXpaths>
</WebElementEntity>
