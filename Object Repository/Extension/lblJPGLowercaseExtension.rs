<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>for lower case</description>
   <name>lblJPGLowercaseExtension</name>
   <tag></tag>
   <elementGuidId>8d3b8623-7ef7-40c4-9c7f-f97f08f08f6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(@title, '.jpg')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>.jpg</value>
   </webElementProperties>
</WebElementEntity>
