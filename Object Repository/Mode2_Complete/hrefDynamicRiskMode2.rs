<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hrefDynamicRiskMode2</name>
   <tag></tag>
   <elementGuidId>d7ff58a1-411b-4cfa-aefd-da884fc78a04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[10]/div[2]/table/tbody/tr[2]/td[3]/div/div/table/tbody/tr/td/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fpAttr fpTextAttr</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>v_81_10270_2385</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fp-attrid</name>
      <type>Main</type>
      <value>81_10270_2385</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    

Risks1. Mobile Loan &amp; Mobile Saving PURPokok PembahasanCapacity Planning untuk infrastrukturPotensi ResikoPerformance sistem terhambat dikarenakan besarnya kapasitas penggunaan.Rekomendasi Mitigasi RisikoMelakukan capacity assessment terhadap plan penggunaan dari sistem ini.Memastikan kapasitas server (Sinaya) yang akan digunakan men-cover hasil assessment terhadap kebutuhan kapasitas sistem. Tanggapan Unit Kerja PemohonAkan dilakukan oleh Team IT Solution Architect dan Solution Development terkait kapasitas Server Sinaya yang akan digunakan bersama dengan Mobile SavingLikelihoodImpactImpact DescriptionIT -  AvailabilityRGM RiskModerate to High2. Mobile Loan &amp; Mobile Saving PURPokok PembahasanSpesifikasi tablet untuk agen dan browser yang akan diakses via cabang.Potensi ResikoSistem tidak dapat diakses ataupun tidak dapat berfungsi dengan baik dikarenakan adanya perbedaan spesifikasi antara sistem yang dibangun dengan tablet ataupun browser pada cabangRekomendasi Mitigasi RisikoMemastikan sistem yang dibangun dapat digunakan pada browser cabang. Memastikan spesifikasi tablet yang akan diadakan mendukung APK yang dibangun. Tanggapan Unit Kerja PemohonSistem digunakan pada Tablet Samsung A6, dan sudah di assessment sebelumnyaAplikasi yang dikembangkan juga langsung pada Device terkaitLikelihoodImpactImpact DescriptionIT -  AvailabilityRGM RiskModerate to High3. Mobile Loan &amp; Mobile Saving PUR Go to ActionsPokok PembahasanPengamanan data nasabah yang dikirim dari tablet ke sistem.Potensi ResikoAdanya celah keamanan yang dapat dimanfaatkan oleh pihak yang tidak berkepentingan untuk mendapatkan data confidential.Rekomendasi Mitigasi RisikoAgar memastikan adanya enkripsi pada jalur pengiriman data.Agar dilakukan penetration test terhadap jalur distribusi data tersebut. Untuk scope mohon konsultasi dengan IT Security.Tanggapan Unit Kerja PemohonSedang dilakukan untuk assessment APN Private untuk komunikasi data antara Tablet ke Pusat (Sentral) oleh Team IT Infrastructure.LikelihoodImpactImpact DescriptionInformation ConfidentialityRGM RiskModerate to High4. Mobile Saving &amp; Mobile Loan PURPokok PembahasanBusiness Impact Analysis untuk sistem e-Form MobilePotensi ResikoTidak adanya penentuan kritikalitas sistem dapat mengakibatkan terhambatnya kegiatan bisnis dikarenakan pemulihan data dan sistem tidak sesuai dengan yang diharapkan oleh bisnis.Rekomendasi Mitigasi RisikoMemastikan pengisian Business Impact Analysis untuk sistem ini. Untuk pengisian BIA dapat menghubungi IT Risk. Tanggapan Unit Kerja PemohonAkan di follow up oleh IT Business Alliance ke Team IT Risk terkait resiko iniLikelihoodImpactImpact DescriptionIT -  AvailabilityRGM RiskModerate to High
    










Edit

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;v_81_10270_2385&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='v_81_10270_2385']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='fp-attrcell-81_10270_2385']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[48]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Risk Assessment'])[4]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div[2]/table/tbody/tr[2]/td[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
