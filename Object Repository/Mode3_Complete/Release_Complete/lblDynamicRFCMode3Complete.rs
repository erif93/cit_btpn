<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblDynamicRFCMode3Complete</name>
   <tag></tag>
   <elementGuidId>f42d1fa4-759e-4d64-95a0-ac8c40717bb9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[8]/div[2]/table/tbody/tr/td[3]/div/div/div/div/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[8]/div[2]/table/tbody/tr/td[3]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fp-richhover-link</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>v_81_21381_2952</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
		
			
			
				
					
						
						   
						   
						   
						       












    
        
        


1095.CRF.--.WOW Improvement 2019.Ticket #INC0027644 - Menu USSD selain PLN yang tidak sesuai, Penambahan log file output untuk SKN dan Re-implementasi Pause, continue dan delete untuk USSD push notification


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1194.CRF.--.WOW Improvement 2019.1


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1212.PIR.--.WOW Improvement 2019.App version 12


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1239.CRF.--.WOW Improvement 2019.2


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1271.CRF.--.WOW Improvement 2019.USSD Push Pesan bisa disimpan ; Hazle Cash ; Bulk Upload Kode Bank


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1279.PIR.--.WOW Improvement 2019.App Version 13


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1297.PIR.--.WOW Improvement 2019.Ticket #INC0040493 - Kendala Menu Upload Assignment Route WEB AMS


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1300.CRF.--.WOW Improvement 2019.Upgrade OS Portal Wow di DR


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1321.CRF.--.WOW Improvement 2019.SPM Enhancement


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1326.CRF.--.WOW Improvement 2019.Transfer Wow iB ke BTPNS dan sebaliknya, SMS Notif Reversal NonTaglist Wow iB, Top up Pulsa Non Jatel Wow iB, Ticket INC0025276 - Kendala Top Up denomkecil Telkomsel via Pendopo &amp;Switch dari Pendopo ke Jatelindo dan sebaliknya, TICKET #1053943  Case saldo hilang saat pengecekan saldo


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1365.CRF.--.WOW Improvement 2019.Ticket #INC0044249 - Tidak Bisa Simpan Trx Trakhir (Mini Statement)


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1391.PIR.--.WOW Improvement 2019.Tiket#INC0054473 - Salto Opex Issue Production


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1405.CRF.--.WOW Improvement 2019.Top Up Pulsa Biller Finnet dan Biller Arta Jasa via Aplikasi Wow Syariah Non HP


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1424.CRF.--.WOW Improvement 2019.Pause, Continue, Stop USSD Push Notifictaion &amp; Housekeeping 4 Table Money Non Transaction


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1441.CRF.--.WOW Improvement 2019.Ticket Problem #1091819 (WDM56-Rek EQ Cust dan Wow Link Bersaldo) dan Ticket #INC0027644 (WDM170-Menu PLN via USSD tidak sesuai)


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1472.CRF.--.WOW Improvement 2019.Ticket #INC0027644 (Menu USSD selain PLN yang tidak sesuai), Penambahan log file output untuk SKN dan Re-implementasi Pause, continue dan delete untuk bulk upload ussd push notification


        
    

						   
						   
						
						   
						   
						   
						       












    
        
        


1489.CRF.--.WOW Improvement 2019.Transaksi Wow iB Non HP untuk Biller Jatelindo (PLN, Top up pulsa, BPJS) dan Tiket INC0014260 -  Transaksi Setor Tunai ke Rek Virtual Account Gagal, Tetapi Mendapatkan SMS Notifikasi berhasil


        
    

						   
						   
						
					
			    	
							
			
		
		
	










Edit

	
	   
     
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[8]/div[2]/table/tbody/tr/td[3]/div/div/div</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='v_81_21381_2952']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='fp-attrcell-81_21381_2952']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure this attribute'])[52]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RFC MODE 3'])[2]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dokumen BAST'])[1]/preceding::div[55]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/table/tbody/tr/td[3]/div/div/div/div/a</value>
   </webElementXpaths>
</WebElementEntity>
