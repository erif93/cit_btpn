<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project_Null_Mode3</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7fcade01-041c-444b-a24c-d16f3fdf38d7</testSuiteGuid>
   <testCaseLink>
      <guid>ec2e543c-7dfe-4879-a180-0227e7614f30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_FocalPoint2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55117fdc-f497-4fe1-a27b-70323fd9422d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mode3/Reject/Go_To_Mode3_ProjectReject</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e723e141-4444-41c4-a837-a86989f33f0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mode3/Reject/Click_And_Download_RejectDocument_Mode3</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
