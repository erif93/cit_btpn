<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project_Null_Mode2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>31b96ad9-9425-4889-bdfb-f31b418de37c</testSuiteGuid>
   <testCaseLink>
      <guid>ec2e543c-7dfe-4879-a180-0227e7614f30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_FocalPoint2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c5ad442-51fe-4362-9e07-c563b2bebb62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mode2/Reject/Go_To_Mode2_ProjectReject</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d8bc391-63fb-4e0d-bdae-e197c1901961</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mode2/Reject/Click_And_Download_RejectDocument_Mode2</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
