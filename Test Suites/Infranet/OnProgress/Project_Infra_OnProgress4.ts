<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project_Infra_OnProgress4</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cd7f6c4d-926a-45cc-88bf-8b697df3b669</testSuiteGuid>
   <testCaseLink>
      <guid>3f26e24b-29c5-4137-8221-680714f589cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_FocalPoint</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>795abbd1-5acc-41ae-876a-cb3b80229423</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/Go_To_InfraOnProgress</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3774233-e315-4e7c-bd34-f63283bb55df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/OnProgress/Download_All_DocumentOnProgress4</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
