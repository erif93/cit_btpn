<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project_Infra_OnProgress9</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>09c6f85d-bd93-4318-a09c-eec26f524807</testSuiteGuid>
   <testCaseLink>
      <guid>3f26e24b-29c5-4137-8221-680714f589cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_FocalPoint</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>795abbd1-5acc-41ae-876a-cb3b80229423</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/Go_To_InfraOnProgress</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3774233-e315-4e7c-bd34-f63283bb55df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/OnProgress/Download_All_DocumentOnProgress9</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
