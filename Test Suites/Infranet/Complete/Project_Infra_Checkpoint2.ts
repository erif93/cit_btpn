<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project_Infra_Checkpoint2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>74a31c89-45f4-4ae2-8ce3-b40168a52641</testSuiteGuid>
   <testCaseLink>
      <guid>e685c1cb-1605-4f47-9823-0b605a9cc611</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_FocalPoint2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d92e02bb-ecfa-4230-b8a5-89f499a744f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/Go_To_InfranetComplete</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56a9b875-7060-4447-a579-85772cfe2286</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Infranet/Complete/Download_All_DocumentInfra_2</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
