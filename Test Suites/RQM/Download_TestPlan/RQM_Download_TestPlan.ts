<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RQM_Download_TestPlan</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b88b6365-63d9-402a-a4db-12a89c6ba482</testSuiteGuid>
   <testCaseLink>
      <guid>4faf6b5e-1c0f-4891-bf2e-61df01d5919e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginPage/Login_RQM_18056767_Driver75</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd71ee6e-2f7c-4824-8bd0-ac9ff8561f92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RQM/Planning/Go_To_Browse_Test_Plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76133e1c-2108-4646-972b-b29edad6b1a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RQM/Planning/Download_And_CheckAttachment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
