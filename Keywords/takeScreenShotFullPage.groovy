import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import java.awt.Color
import java.awt.image.BufferedImage
import java.nio.file.Path
import java.nio.file.Paths

import javax.imageio.ImageIO

import org.apache.commons.io.FileUtils
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.configuration.RunConfiguration

import newFilesHelper.filesHelper
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

import ru.yandex.qatools.ashot.AShot
import ru.yandex.qatools.ashot.Screenshot
import ru.yandex.qatools.ashot.coordinates.Coords
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider
import ru.yandex.qatools.ashot.shooting.ShootingStrategies

import internal.GlobalVariable


public class takeScreenShotFullPage {

	static final String screenshotDir = RunConfiguration.getProjectDir() + File.separator + "Screenshots"
	static final Color ignoredColor = Color.GRAY
	static final int scrollTimeout = 100

	@Keyword
	static void takeEntirePageScreenshot(String filename, FailureHandling flowControl = FailureHandling.OPTIONAL){
		try{
			def screenshot = new AShot()
					.shootingStrategy(ShootingStrategies.viewportPasting(scrollTimeout))
					.takeScreenshot(DriverFactory.getWebDriver())
			saveScreenshot(filename, screenshot)
		}catch(e){
			e.printStackTrace()
		}
	}



	@Keyword
	static void takeWebElementScreenshot(TestObject object, String filename, int timeout = 10, FailureHandling flowControl = FailureHandling.OPTIONAL){
		try{
			def driver = DriverFactory.getWebDriver()

			def element = WebUiCommonHelper.findWebElement(object, timeout)

			def screenshot = new AShot()
					.shootingStrategy(ShootingStrategies.viewportPasting(scrollTimeout))
					.takeScreenshot(driver, element)
			saveScreenshot(filename, screenshot)
		}catch(e){
			e.printStackTrace()
		}
	}


	private static void saveScreenshot(String filename, Screenshot screenshot){
		String workingFolder = RunConfiguration.getProjectDir()
		String extension = filesHelper.getFileExtension(filename)

		Path screenshotPath = Paths.get(screenshotDir)

		if(!screenshotPath.toFile().exists()){
			screenshotPath.toFile().mkdir()
		}
		ImageIO.write(screenshot.getImage(), extension, screenshotPath.resolve(filename).toFile())
	}
}
